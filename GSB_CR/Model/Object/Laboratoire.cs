﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSB_CR.Model.Object
{
    class Laboratoire
    {
        //Propriétés
        string LabCode;
        string LabNom;
        string LabChefVente;

        //Constructeur par défaut
        public Laboratoire() { }
        
        //Constructeur paramétré
        public Laboratoire(string labCode, string labNom, string labChefVente)
        {
            LabCode      = labCode;
            LabNom       = labNom;
            LabChefVente = labChefVente;
        }

        //Accesseurs
        public string getLabCode()
        {
            return LabCode;
        }

        public string getLabNom()
        {
            return LabNom;
        }

        public string getLabChefVente()
        {
            return LabChefVente;
        }

        //Mutateurs
        public void setLabCode(string value)
        {
            LabCode = value;
        }

        public void setLabNom(string value)
        {
            LabNom = value;
        }

        public void setLabChefVente(string value)
        {
            LabChefVente = value;
        }

        //Réécriture du ToString()
        public override string ToString()
        {
            return LabNom;
        }
    }
}
