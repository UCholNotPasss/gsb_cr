﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSB_CR.Model.Object
{
    class Medicament
    {
        //Propriétés
        string DepotLegal;
        string NomCommercial;
        string CodeFamille;
        Famille Famille;
        string Composition;
        string Effets;
        string ContreIndic;
        float PrixEchantillon;

        //Constructeur par défaut
        public Medicament() { }

        //Constructeur paramétré sans objets
        public Medicament(string depotLegal, string nomCommercial, string codeFamille, string composition, string effets, string contreIndic, float prixEchantillon)
        {
            DepotLegal = depotLegal;
            NomCommercial = nomCommercial;
            CodeFamille = codeFamille;
            Composition = composition;
            Effets = effets;
            ContreIndic = contreIndic;
            PrixEchantillon = prixEchantillon;
        }

        //Constructeur paramétré avec objets
        public Medicament(string depotLegal, string nomCommercial, string codeFamille, Famille famille, string composition, string effets, string contreIndic, float prixEchantillon)
        {
            DepotLegal = depotLegal;
            NomCommercial = nomCommercial;
            CodeFamille = codeFamille;
            Famille = famille;
            Composition = composition;
            Effets = effets;
            ContreIndic = contreIndic;
            PrixEchantillon = prixEchantillon;
        }

        //Accesseurs
        public string getDepotLegal()
        {
            return DepotLegal;
        }

        public string getNomCommercial()
        {
            return NomCommercial;
        }

        public string getCodeFamille()
        {
            return CodeFamille;
        }

        public Famille getFamille()
        {
            return Famille;
        }

        public string getComposition()
        {
            return Composition;
        }

        public string getEffets()
        {
            return Effets;
        }

        public string getContreIndic()
        {
            return ContreIndic;
        }

        public float getPrixEchantillon()
        {
            return PrixEchantillon;
        }

        //Mutateurs
        public void setDepotLegal(string value)
        {
            DepotLegal = value;
        }

        public void setNomCommercial(string value)
        {
            NomCommercial = value;
        }

        public void setCodeFamille(string value)
        {
            CodeFamille = value;
        }

        public void setFamille(Famille value)
        {
            Famille = value;
        }

        public void setComposition(string value)
        {
            Composition = value;
        }

        public void setEffets(string value)
        {
            Effets = value;
        }

        public void setContreIndic(string value)
        {
            ContreIndic = value;
        }

        public void setPrixEchantillon(float value)
        {
            PrixEchantillon = value;
        }

        //Réécriture du ToString()
        public override string ToString()
        {
            return NomCommercial;
        }
    }
}
