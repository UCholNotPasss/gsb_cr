﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSB_CR.Model.Object
{
    class TypePraticien
    {
        //Propriétés
        string Code;
        string Libelle;
        string Lieu;

        //Constructeur par défaut
        public TypePraticien() { }

        //Constructeur paramétré sans objects
        public TypePraticien(string code, string libelle, string lieu)
        {
            Code = code;
            Libelle = libelle;
            Lieu = lieu;
        }

        //Accesseurs
        public string getCode()
        {
            return Code;
        }

        public string getLibelle()
        {
            return Libelle;
        }

        public string getLieu()
        {
            return Lieu;
        }

        //Mutateurs
        public void setCode(string value)
        {
            Code = value;
        }

        public void setLibelle(string value)
        {
            Libelle = value;
        }

        public void setLieu(string value)
        {
            Lieu = value;
        }

        //Réécriture du ToString()
        public override string ToString()
        {
            return Libelle;
        }
    }
}
