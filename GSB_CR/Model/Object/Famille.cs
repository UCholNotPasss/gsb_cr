﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSB_CR.Model.Object
{
    class Famille
    {
        //Propriétés
        string FamCode;
        string FamLibelle;

        //Constructeur par défaut
        public Famille() { }

        //Constructeur paramétré
        public Famille(string famCode, string famLibelle)
        {
            FamCode = famCode;
            FamLibelle = famLibelle;
        }

        //Accesseurs
        public string getFamCode()
        {
            return FamCode;
        }

        public string getFamLibelle()
        {
            return FamLibelle;
        }

        //Mutateurs
        public void setFamCode(string value)
        {
            FamCode = value;
        }

        public void setFamLibelle(string value)
        {
            FamLibelle = value;
        }

        //Réécriture du ToString()
        public override string ToString()
        {
            return FamLibelle;
        }
    }
}
