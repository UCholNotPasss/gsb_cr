﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSB_CR.Model.Object
{
    class Groupe
    {
        //Propriétés
        int Id;
        string Nom;

        //Constructeur par défaut
        public Groupe() { }

        //Constructeur paramétré
        public Groupe(int id, string nom)
        {
            Id = id;
            Nom = nom;
        }

        //Accesseurs
        public int getId()
        {
            return Id;
        }

        public string getNom()
        {
            return Nom;
        }

        //Mutateurs
        public void setId(int value)
        {
            Id = value;
        }

        public void setNom(string value)
        {
            Nom = value;
        }

        //Réécriture du ToString()
        public override string ToString()
        {
            return Nom;
        }
    }
}
