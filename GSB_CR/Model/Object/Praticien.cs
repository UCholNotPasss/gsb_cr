﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSB_CR.Model.Object
{
    class Praticien
    {
        //Propriétés
        int Num;
        string Nom;
        string Prenom;
        string Adresse;
        string CP;
        string Ville;
        float CoefNotoriete;
        float CoefConfiance;
        string TypCode;
        TypePraticien TypePraticien;

        //Constructeur par défaut
        public Praticien(){ }

        //Constructeur paramétré sans objects
        public Praticien(int num, string nom, string prenom, string adresse, string cp, string ville, float coefNotoriete, float coefConfiance, string typCode)
        {
            Num           = num;
            Nom           = nom;
            Prenom        = prenom;
            Adresse       = adresse;
            CP            = cp;
            Ville         = ville;
            CoefNotoriete = coefNotoriete;
            CoefConfiance = coefConfiance;
            TypCode       = typCode;
        }

        //Constructeur paramétré avec objects
        public Praticien(int num, string nom, string prenom, string adresse, string cp, string ville, float coefNotoriete, float coefConfiance, string typCode, TypePraticien typePraticien)
        {
            Num = num;
            Nom = nom;
            Prenom = prenom;
            Adresse = adresse;
            CP = cp;
            Ville = ville;
            CoefNotoriete = coefNotoriete;
            CoefConfiance = coefConfiance;
            TypCode = typCode;
            TypePraticien = typePraticien;
        }

        //Accesseurs
        public int getNum()
        {
            return Num;
        }

        public string getNom()
        {
            return Nom;
        }

        public string getPrenom()
        {
            return Prenom;
        }

        public string getAdresse()
        {
            return Adresse;
        }

        public string getCP()
        {
            return CP;
        }

        public string getVille()
        {
            return Ville;
        }

        public float getCoefNotoriete()
        {
            return CoefNotoriete;
        }

        public float getCoefConfiance()
        {
            return CoefConfiance;
        }

        public string getTypCode()
        {
            return TypCode;
        }

        public TypePraticien getTypePraticien()
        {
            return TypePraticien;
        }

        //Mutateurs
        public void setNum(int value)
        {
            Num = value;
        }

        public void setNom(string value)
        {
            Nom = value;
        }

        public void setPrenom(string value)
        {
            Prenom = value;
        }

        public void setAdresse(string value)
        {
            Adresse = value;
        }

        public void setCP(string value)
        {
            CP = value;
        }

        public void setVille(string value)
        {
            Ville = value;
        }

        public void setCoefNotoriete(float value)
        {
            CoefNotoriete = value;
        }

        public void setCoefConfiance(float value)
        {
            CoefConfiance = value;
        }

        public void setTypCode(string value)
        {
            TypCode = value;
        }

        public void setTypePraticien(TypePraticien value)
        {
            TypePraticien = value;
        }

        //Réécriture du ToString()
        public override string ToString()
        {
            return Nom + " " + Prenom;
        }
    }
}
