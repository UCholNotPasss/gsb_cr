﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSB_CR.Model.Object
{
    class Affecter
    {
        //Propriétés
        int IdGroupe;
        string Matricule;
        Groupe Groupe;
        Collaborateur Collaborateur;

        //Constructeur par défaut
        public Affecter() { }

        //Constructeur paramétré sans objets
        public Affecter(int idGroupe, string matricule)
        {
            IdGroupe = idGroupe;
            Matricule = matricule;
        }

        //Constructeur paramétré avec objets
        public Affecter(int idGroupe, string matricule, Groupe groupe, Collaborateur collaborateur)
        {
            IdGroupe = idGroupe;
            Matricule = matricule;
            Groupe = groupe;
            Collaborateur = collaborateur;
        }

        //Accesseurs
        public int getIdGroupe()
        {
            return IdGroupe;
        }

        public string getMatricule()
        {
            return Matricule;
        }

        public Groupe getGroupe()
        {
            return Groupe;
        }

        public Collaborateur getCollaborateur()
        {
            return Collaborateur;
        }

        //Mutateurs
        public void setIdGroupe(int value)
        {
            IdGroupe = value;
        }

        public void setMatricule(string value)
        {
            Matricule = value;
        }

        public void setGroupe(Groupe value)
        {
            Groupe = value;
        }

        public void setCollaborateur(Collaborateur value)
        {
            Collaborateur = value;
        }
    }
}
