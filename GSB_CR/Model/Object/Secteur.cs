﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSB_CR.Model.Object
{
    class Secteur
    {
        //Propriétés
        string SecCode;
        string SecLibelle;

        //Constructeur par défaut
        public Secteur() { }

        //Constructeur paramétré
        public Secteur(string secCode, string secLibelle)
        {
            SecCode    = secCode;
            SecLibelle = secLibelle;
        }

        //Accesseurs
        public string getSecCode()
        {
            return SecCode;
        }

        public string getSecLibelle()
        {
            return SecLibelle;
        }

        //Mutateurs
        public void setSecCode(string value)
        {
            SecCode = value;
        }

        public void setSecLibelle(string value)
        {
            SecLibelle = value;
        }

        //Réécriture du ToString()
        public override string ToString()
        {
            return SecLibelle;
        }
    }
}
