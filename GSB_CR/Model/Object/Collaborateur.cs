﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSB_CR.Model.Object
{
    class Collaborateur
    {
        //Propriétés
        string Matricule;
        string Nom;
        string Prenom;
        string Adresse;
        string CP;
        string Ville;
        DateTime DateEmbauche;
        string CodeSecteur;
        Secteur Secteur;
        string CodeLaboratoire;
        Laboratoire Laboratoire;
        string Login;
        string MotDePasse;

        //Constructeur par défaut
        public Collaborateur(){ }

        //Constructeur paramétré sans objects
        public Collaborateur(string matricule, string nom, string prenom, string adresse, string cp, string ville, 
                                DateTime dateEmbauche, string codeSecteur, string codeLaboratoire, string login, string motDePasse)
        {
            Matricule       = matricule;
            Nom             = nom;
            Prenom          = prenom;
            Adresse         = adresse;
            CP              = cp;
            Ville           = ville;
            DateEmbauche    = dateEmbauche;
            CodeSecteur     = codeSecteur;
            CodeLaboratoire = codeLaboratoire;
            Login = login;
            MotDePasse = motDePasse;
        }

        //Constructeur paramétré avec objets
        public Collaborateur(string matricule, string nom, string prenom, string adresse, string cp, string ville, DateTime dateEmbauche,
                                string codeSecteur, Secteur secteur, string codeLaboratoire, Laboratoire laboratoire, string login, string motDePasse)
        {
            Matricule       = matricule;
            Nom             = nom;
            Prenom          = prenom;
            Adresse         = adresse;
            CP              = cp;
            Ville           = ville;
            DateEmbauche    = dateEmbauche;
            CodeSecteur     = codeSecteur;
            Secteur         = secteur;
            CodeLaboratoire = codeLaboratoire;
            Laboratoire     = laboratoire;
            Login = login;
            MotDePasse = motDePasse;
        }

        //Accesseurs
        public string getMatricule()
        {
            return Matricule;
        }

        public string getNom()
        {
            return Nom;
        }

        public string getPrenom()
        {
            return Prenom;
        }

        public string getAdresse()
        {
            return Adresse;
        }

        public string getCP()
        {
            return CP;
        }

        public string getVille()
        {
            return Ville;
        }

        public DateTime getDateEmbauche()
        {
            return DateEmbauche;
        }

        public string getCodeSecteur()
        {
            return CodeSecteur;
        }

        public Secteur getSecteur()
        {
            return Secteur;
        }

        public string getCodeLaboratoire()
        {
            return CodeLaboratoire;
        }

        public Laboratoire getLaboratoire()
        {
            return Laboratoire;
        }

        public string getLogin()
        {
            return Login;
        }

        public string getMotDePasse()
        {
            return MotDePasse;
        }

        //Mutateurs
        public void setMatricule(string value)
        {
            Matricule = value;
        }

        public void setNom(string value)
        {
            Nom = value;
        }

        public void setPrenom(string value)
        {
            Prenom = value;
        }

        public void setAdresse(string value)
        {
            Adresse = value;
        }

        public void setCP(string value)
        {
            CP = value;
        }

        public void setVille(string value)
        {
            Ville = value;
        }

        public void setDateEmbauche(DateTime value)
        {
            DateEmbauche = value;
        }

        public void setCodeSecteur(string value)
        {
            CodeSecteur = value;
        }

        public void setSecteur(Secteur value)
        {
            Secteur = value;
        }

        public void setCodeLaboratoire(string value)
        {
            CodeLaboratoire = value;
        }

        public void setLaboratoire(Laboratoire value)
        {
            Laboratoire = value;
        }

        public void setLogin(string value)
        {
            Login = value;
        }

        public void setMotDePasse(string value)
        {
            MotDePasse = value;
        }

        //Réécriture du ToString()
        public override string ToString()
        {
            return Nom + " " + Prenom;
        }
    }
}
