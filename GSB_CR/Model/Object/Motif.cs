﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSB_CR.Model.Object
{
    class Motif
    {
        //Propriétés
        int Id;
        string Libelle;

        //Constructeur par défaut
        public Motif() { }

        //Constructeur paramétré
        public Motif(int id, string libelle)
        {
            Id = id;
            Libelle = libelle;
        }

        //Accesseurs
        public int getId()
        {
            return Id;
        }

        public string getLibelle()
        {
            return Libelle;
        }

        //Mutateurs
        public void setId(int value)
        {
            Id = value;
        }

        public void setLibelle(string value)
        {
            Libelle = value;
        }

        //Réécriture du ToString()
        public override string ToString()
        {
            return Libelle;
        }
    }
}
