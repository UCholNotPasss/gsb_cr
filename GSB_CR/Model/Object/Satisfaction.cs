﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSB_CR.Model.Object
{
    class Satisfaction
    {
        /***
         * Créer un objet Satisfaction avec Accesseurs et Mutateurs
         **/
        public int Note;
        public int NumPraticien;
        public int NumRapport;
        public string Matricule;
        public int IdQuestion;
        public DateTime Date;

        public Satisfaction() { }

        public Satisfaction(int note, int numPraticien, int numRapport, string matricule, int idQuestion, DateTime date)
        {
            Note = note;
            NumPraticien = numPraticien;
            NumRapport = numRapport;
            Matricule = matricule;
            IdQuestion = idQuestion;
            Date = date;
        }
    }
}
