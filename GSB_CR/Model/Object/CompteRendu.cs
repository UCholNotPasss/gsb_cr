﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GSB_CR.Model.Object
{
    class CompteRendu
    {
        //Propriétés
        string Matricule;
        int Num;
        int NumPraticien;
        DateTime Date;
        DateTime DateVisite;
        string Bilan;
        int IdMotif;
        Collaborateur Collaborateur;
        Praticien Praticien;
        Motif Motif;

        //Constructeur par défaut
        public CompteRendu() { }

        //Constructeur paramétré sans objets
        public CompteRendu(string matricule, int num, int numPraticien, DateTime date, DateTime dateVisite, string bilan, int idMotif)
        {
            Matricule = matricule;
            Num = num;
            NumPraticien = numPraticien;
            Date = date;
            DateVisite = dateVisite;
            Bilan = bilan;
            IdMotif = idMotif;
        }

        //Constructeur paramétré avec objets
        public CompteRendu(string matricule, int num, int numPraticien, DateTime date, DateTime dateVisite, string bilan, int idMotif, Collaborateur collaborateur, Praticien praticien, Motif motif)
        {
            Matricule = matricule;
            Num = num;
            NumPraticien = numPraticien;
            Date = date;
            DateVisite = dateVisite;
            Bilan = bilan;
            IdMotif = idMotif;
            Collaborateur = collaborateur;
            Praticien = praticien;
            Motif = motif;
        }

        //Accesseurs
        public string getMatricule()
        {
            return Matricule;
        }

        public int getNum()
        {
            return Num;
        }

        public int getNumPraticien()
        {
            return NumPraticien;
        }

        public DateTime getDate()
        {
            return Date;
        }

        public DateTime getDateVisite()
        {
            return DateVisite;
        }

        public string getBilan()
        {
            return Bilan;
        }
        public int getIdMotif()
        {
            return IdMotif;
        }
        public Collaborateur getCollaborateur()
        {
            return Collaborateur;
        }

        public Praticien getPraticien()
        {
            return Praticien;
        }
        public Motif getMotif()
        {
            return Motif;
        }

        //Mutateurs
        public void setMatricule(string value)
        {
            Matricule = value;
        }

        public void setNum(int value)
        {
            Num = value;
        }

        public void setNumPraticien(int value)
        {
            NumPraticien = value;
        }

        public void setDate(DateTime value)
        {
            Date = value;
        }

        public void setDateVisite(DateTime value)
        {
            DateVisite = value;
        }

        public void setBilan(string value)
        {
            Bilan = value;
        }
        public void setIdMotif(int value)
        {
            IdMotif = value;
        }
        public void setCollaborateur(Collaborateur value)
        {
            Collaborateur = value;
        }

        public void setPraticien(Praticien value)
        {
            Praticien = value;
        }
        public void setMotif(Motif value)
        {
            Motif = value;
        }
    }
}
