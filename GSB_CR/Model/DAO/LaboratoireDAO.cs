﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using GSB_CR.Model.Object;

namespace GSB_CR.Model.DAO
{
    class LaboratoireDAO : AbstractDAO
    {
        //Récupérer un laboratoire
        public Laboratoire Find(string code)
        {
            Laboratoire laboratoire = null;
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM LABO WHERE LAB_CODE = @code;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@code", code);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string nom = dr.GetString("LAB_NOM");
                    string chefVente = dr.GetString("LAB_CHEFVENTE");
                    laboratoire = new Laboratoire(code, nom, chefVente);
                }
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return laboratoire;
        }

        //Récupérer tous les laboratoires
        public List<Laboratoire> FindAll()
        {
            List<Laboratoire> laboratoires = new List<Laboratoire>();
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM LABO;";
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string code = dr.GetString("LAB_CODE");
                    string nom = dr.GetString("LAB_NOM");
                    string chefVente = dr.GetString("LAB_CHEFVENTE");
                    laboratoires.Add(new Laboratoire(code, nom, chefVente));
                }
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return laboratoires;
        }
    }
}
