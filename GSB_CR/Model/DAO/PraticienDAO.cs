﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using GSB_CR.Model.Object;

namespace GSB_CR.Model.DAO
{
    class PraticienDAO : AbstractDAO
    {
        //Récupérer tous les praticiens
        public List<Praticien> FindAll()
        {
            List<Praticien> praticiens = new List<Praticien>();
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM PRATICIEN p INNER JOIN TYPE_PRATICIEN tp ON p.TYP_CODE = tp.TYP_CODE ORDER BY PRA_NOM, PRA_PRENOM;";
                cmd.Prepare();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int num = dr.GetInt32("PRA_NUM");
                    string nom = dr.GetString("PRA_NOM");
                    string prenom = dr.GetString("PRA_PRENOM");
                    string adresse = dr.GetString("PRA_ADRESSE");
                    string cp = dr.GetString("PRA_CP");
                    string ville = dr.GetString("PRA_VILLE");
                    float coefNotoriete = dr.GetFloat("PRA_COEFNOTORIETE");
                    float coefConfiance = dr.GetFloat("PRA_COEFCONFIANCE");
                    string typCode = dr.GetString("TYP_CODE");
                    string libelle = dr.GetString("TYP_LIBELLE");
                    string lieu = dr.GetString("TYP_LIEU");
                    TypePraticien typePraticien = new TypePraticien(typCode, libelle, lieu);
                    praticiens.Add(new Praticien(num, nom, prenom, adresse, cp, ville, coefNotoriete, coefConfiance, typCode, typePraticien));
                }
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return praticiens;
        }

        //Récupérer un praticien
        public Praticien Find(int num)
        {
            Praticien praticien = null;
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM PRATICIEN WHERE PRA_NUM = @num;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@num", num);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string nom = dr.GetString("PRA_NOM");
                    string prenom = dr.GetString("PRA_PRENOM");
                    string adresse = dr.GetString("PRA_ADRESSE");
                    string cp = dr.GetString("PRA_CP");
                    string ville = dr.GetString("PRA_VILLE");
                    float coefNotoriete = dr.GetFloat("PRA_COEFNOTORIETE");
                    float coefConfiance = dr.GetFloat("PRA_COEFCONFIANCE");
                    string typCode = dr.GetString("TYP_CODE");
                    praticien = new Praticien(num, nom, prenom, adresse, cp, ville, coefNotoriete, coefConfiance, typCode);
                }
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return praticien;
        }

        //Ajouter un praticien
        public void Insert(Praticien praticien)
        {
            try
            {
                int id = LastInsertId() + 1;
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "INSERT INTO PRATICIEN VALUES (@num, @nom, @prenom, @adresse, @cp, @ville, @notoriete, @confiance, @type);";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@num", id);
                cmd.Parameters.AddWithValue("@nom", praticien.getNom());
                cmd.Parameters.AddWithValue("@prenom", praticien.getPrenom());
                cmd.Parameters.AddWithValue("@adresse", praticien.getAdresse());
                cmd.Parameters.AddWithValue("@cp", praticien.getCP());
                cmd.Parameters.AddWithValue("@ville", praticien.getVille());
                cmd.Parameters.AddWithValue("@notoriete", praticien.getCoefNotoriete());
                cmd.Parameters.AddWithValue("@confiance", praticien.getCoefConfiance());
                cmd.Parameters.AddWithValue("@type", praticien.getTypCode());
                cmd.ExecuteNonQuery();
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
        }

        //Supprimer un praticien
        public void Delete(Praticien praticien)
        {
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM PRATICIEN WHERE PRA_NUM = @num;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@num", praticien.getNum());
                cmd.ExecuteNonQuery();
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
        }

        //Modifier un praticien
        public void Update(Praticien praticien)
        {
            try
            {                
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE PRATICIEN SET PRA_NOM = @nom, PRA_PRENOM = @prenom, PRA_ADRESSE = @adresse, PRA_CODEPOSTAL = @cp, PRA_VILLE = @ville, PRA_COEFNOTORIETE = @notoriete, PRA_COEFCONFIANCE = @confiance, TYP_CODE = @type WHERE PRA_NUM = @num;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@nom", praticien.getNom());
                cmd.Parameters.AddWithValue("@prenom", praticien.getPrenom());
                cmd.Parameters.AddWithValue("@adresse", praticien.getAdresse());
                cmd.Parameters.AddWithValue("@cp", praticien.getCP());
                cmd.Parameters.AddWithValue("@ville", praticien.getVille());
                cmd.Parameters.AddWithValue("@notoriete", praticien.getCoefNotoriete());
                cmd.Parameters.AddWithValue("@confiance", praticien.getCoefConfiance());
                cmd.Parameters.AddWithValue("@type", praticien.getTypCode());
                cmd.Parameters.AddWithValue("@num", praticien.getNum());
                cmd.ExecuteNonQuery();
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
        }

        //Récupérer le dernier identifiant
        public int LastInsertId()
        {
            int id = 0;
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT MAX(pra_num) FROM PRATICIEN;";
                MySqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                    id = dr.GetInt32("MAX(pra_num)");
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return id;
        }
    }
}
