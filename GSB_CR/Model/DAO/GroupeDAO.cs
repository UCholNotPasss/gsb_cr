﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using GSB_CR.Model.Object;

namespace GSB_CR.Model.DAO
{
    class GroupeDAO : AbstractDAO
    {
        //Récupérer toutes les familles
        public List<Groupe> FindAll()
        {
            List<Groupe> groupes = new List<Groupe>();
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM GROUPE;";
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int id = dr.GetInt32("id");
                    string nom = dr.GetString("nom");
                    groupes.Add(new Groupe(id, nom));
                }
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return groupes;
        }
    }
}
