﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GSB_CR.Model.Object;
using MySql.Data.MySqlClient;

namespace GSB_CR.Model.DAO
{
    class CompteRenduDAO : AbstractDAO
    {
        //Ajouter un compte-rendu
        public void Insert(CompteRendu compteRendu)
        {
            compteRendu.setNum(LastInsertId(compteRendu.getMatricule())+1);
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "INSERT INTO rapport_visite VALUES (@matricule, @num, @numPra, @date, @dateVisite, @bilan, @idMotif);";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@matricule", compteRendu.getMatricule());
                cmd.Parameters.AddWithValue("@num", compteRendu.getNum());
                cmd.Parameters.AddWithValue("@numPra", compteRendu.getNumPraticien());
                cmd.Parameters.AddWithValue("@date", compteRendu.getDate());
                cmd.Parameters.AddWithValue("@dateVisite", compteRendu.getDateVisite());
                cmd.Parameters.AddWithValue("@bilan", compteRendu.getBilan());
                cmd.Parameters.AddWithValue("@idMotif", compteRendu.getIdMotif());
                cmd.ExecuteNonQuery();
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
        }

        //Modifier un compte-rendu
        public void Update(CompteRendu compteRendu)
        {
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE rapport_visite SET PRA_NUM = @numPra, RAP_DATE = @date, VIS_DATE = @dateVisite, RAP_BILAN = @bilan, RAP_MOTIF = @idMotif WHERE RAP_NUM = @num AND COL_MATRICULE = @matricule;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@matricule", compteRendu.getMatricule());
                cmd.Parameters.AddWithValue("@num", compteRendu.getNum());
                cmd.Parameters.AddWithValue("@numPra", compteRendu.getNumPraticien());
                cmd.Parameters.AddWithValue("@date", compteRendu.getDate());
                cmd.Parameters.AddWithValue("@dateVisite", compteRendu.getDateVisite());
                cmd.Parameters.AddWithValue("@bilan", compteRendu.getBilan());
                cmd.Parameters.AddWithValue("@idMotif", compteRendu.getIdMotif());
                cmd.ExecuteNonQuery();
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
        }

        //Supprimer un compte-rendu
        public void Delete(CompteRendu compteRendu)
        {
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM rapport_visite WHERE RAP_NUM = @num AND COL_MATRICULE = @matricule;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@matricule", compteRendu.getMatricule());
                cmd.Parameters.AddWithValue("@num", compteRendu.getNum());
                cmd.ExecuteNonQuery();
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
        }

        //Récupérer le dernier identifiant inséré
        public int LastInsertId(string matricule)
        {
            int id = 0;
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT MAX(rap_num) FROM RAPPORT_VISITE WHERE COL_MATRICULE = @matricule;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@matricule", matricule);
                MySqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                    id = dr.GetInt32("MAX(rap_num)");
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return id;
        }

        //Récupérer tous les compte-rendus
        public List<CompteRendu> ReadAll()
        {
            List<CompteRendu> compteRendus = new List<CompteRendu>();
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM RAPPORT_VISITE rv INNER JOIN COLLABORATEUR c ON c.COL_MATRICULE = rv.COL_MATRICULE INNER JOIN PRATICIEN p ON p.PRA_NUM = rv.PRA_NUM INNER JOIN MOTIF m ON m.ID = rv.RAP_MOTIF;";
                MySqlDataReader dr = cmd.ExecuteReader();                
                while(dr.Read())
                {
                    string matricule = dr.GetString("COL_MATRICULE");
                    int num = dr.GetInt32("RAP_NUM");
                    int numPra = dr.GetInt32("PRA_NUM");
                    DateTime date = dr.GetDateTime("RAP_DATE");
                    DateTime dateVisite = dr.GetDateTime("VIS_DATE");
                    string billan = dr.GetString("RAP_BILAN");
                    int idMotif = dr.GetInt32("RAP_MOTIF");
                    CollaborateurDAO collaborateurDAO = new CollaborateurDAO();
                    Collaborateur collaborateur = collaborateurDAO.Find(matricule);
                    PraticienDAO praticienDAO = new PraticienDAO();
                    Praticien praticien = praticienDAO.Find(numPra);
                    MotifDAO motifDAO = new MotifDAO();
                    Motif motif = motifDAO.Find(idMotif);
                    CompteRendu compteRendu = new CompteRendu(matricule, num, numPra, date, dateVisite, billan, idMotif, collaborateur, praticien, motif);
                    compteRendus.Add(compteRendu);
                }
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return compteRendus;
        }

        //Récupérer tous les compte-rendus d'un collaborateur
        public List<CompteRendu> ReadAllByCollaborateur(Collaborateur c)
        {
            List<CompteRendu> compteRendus = new List<CompteRendu>();
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM RAPPORT_VISITE rv INNER JOIN COLLABORATEUR c ON c.COL_MATRICULE = rv.COL_MATRICULE INNER JOIN PRATICIEN p ON p.PRA_NUM = rv.PRA_NUM INNER JOIN MOTIF m ON m.ID = rv.RAP_MOTIF WHERE rv.COL_MATRICULE = @mat;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@mat", c.getMatricule());
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string matricule = dr.GetString("COL_MATRICULE");
                    int num = dr.GetInt32("RAP_NUM");
                    int numPra = dr.GetInt32("PRA_NUM");
                    DateTime date = dr.GetDateTime("RAP_DATE");
                    DateTime dateVisite = dr.GetDateTime("VIS_DATE");
                    string billan = dr.GetString("RAP_BILAN");
                    int idMotif = dr.GetInt32("RAP_MOTIF");
                    CollaborateurDAO collaborateurDAO = new CollaborateurDAO();
                    Collaborateur collaborateur = collaborateurDAO.Find(matricule);
                    PraticienDAO praticienDAO = new PraticienDAO();
                    Praticien praticien = praticienDAO.Find(numPra);
                    MotifDAO motifDAO = new MotifDAO();
                    Motif motif = motifDAO.Find(idMotif);
                    CompteRendu compteRendu = new CompteRendu(matricule, num, numPra, date, dateVisite, billan, idMotif, collaborateur, praticien, motif);
                    compteRendus.Add(compteRendu);
                }
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return compteRendus;
        }

        //Récupérer un compte-rendu
        public CompteRendu Find(int num, string matricule)
        {
            CompteRendu compteRendu = null;
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM RAPPORT_VISITE rv INNER JOIN COLLABORATEUR c ON c.COL_MATRICULE = rv.COL_MATRICULE INNER JOIN PRATICIEN p ON p.PRA_NUM = rv.PRA_NUM INNER JOIN MOTIF m ON m.ID = rv.RAP_MOTIF WHERE RAP_NUM = @num AND rv.COL_MATRICULE = @matricule;";
                cmd.Parameters.AddWithValue("@num", num);
                cmd.Parameters.AddWithValue("@matricule", matricule);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int numPra = dr.GetInt32("PRA_NUM");
                    DateTime date = dr.GetDateTime("RAP_DATE");
                    DateTime dateVisite = dr.GetDateTime("VIS_DATE");
                    string billan = dr.GetString("RAP_BILAN");
                    int idMotif = dr.GetInt32("RAP_MOTIF");
                    CollaborateurDAO collaborateurDAO = new CollaborateurDAO();
                    Collaborateur collaborateur = collaborateurDAO.Find(matricule);
                    PraticienDAO praticienDAO = new PraticienDAO();
                    Praticien praticien = praticienDAO.Find(numPra);
                    MotifDAO motifDAO = new MotifDAO();
                    Motif motif = motifDAO.Find(idMotif);
                    compteRendu = new CompteRendu(matricule, num, numPra, date, dateVisite, billan, idMotif, collaborateur, praticien, motif);
                }
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return compteRendu;
        }

        //Récupérer tous les compte-rendus d'un praticien
        public List<CompteRendu> ReadAllByPraticien(Praticien p)
        {
            List<CompteRendu> compteRendus = new List<CompteRendu>();
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM RAPPORT_VISITE rv INNER JOIN COLLABORATEUR c ON c.COL_MATRICULE = rv.COL_MATRICULE INNER JOIN PRATICIEN p ON p.PRA_NUM = rv.PRA_NUM INNER JOIN MOTIF m ON m.ID = rv.RAP_MOTIF WHERE rv.pra_num = @num;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@num", 1);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string matricule = dr.GetString("COL_MATRICULE");
                    int num = dr.GetInt32("RAP_NUM");
                    int numPra = dr.GetInt32("PRA_NUM");
                    DateTime date = dr.GetDateTime("RAP_DATE");
                    DateTime dateVisite = dr.GetDateTime("VIS_DATE");
                    string billan = dr.GetString("RAP_BILAN");
                    int idMotif = dr.GetInt32("RAP_MOTIF");
                    CollaborateurDAO collaborateurDAO = new CollaborateurDAO();
                    Collaborateur collaborateur = collaborateurDAO.Find(matricule);
                    PraticienDAO praticienDAO = new PraticienDAO();
                    Praticien praticien = praticienDAO.Find(numPra);
                    MotifDAO motifDAO = new MotifDAO();
                    Motif motif = motifDAO.Find(idMotif);
                    CompteRendu compteRendu = new CompteRendu(matricule, num, numPra, date, dateVisite, billan, idMotif, collaborateur, praticien, motif);
                    compteRendus.Add(compteRendu);
                }
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return compteRendus;
        }
    }
}
