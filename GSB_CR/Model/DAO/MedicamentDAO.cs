﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GSB_CR.Model.DAO;
using GSB_CR.Model.Object;
using MySql.Data.MySqlClient;

namespace GSB_CR.Model.DAO
{
    class MedicamentDAO : AbstractDAO
    {
        //Récupérer un médicament
        public Medicament Find(string depotLegal)
        {
            Medicament medicament = null;
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM MEDICAMENT WHERE MED_DEPOTLEGAL = @depot;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@depot", depotLegal);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string nomCommercial = dr.GetString("MED_NOMCOMMERCIAL");
                    string famCode = dr.GetString("FAM_CODE");
                    FamilleDAO familleDAO = new FamilleDAO();
                    Famille famille = familleDAO.Find(famCode);
                    string composition = dr.GetString("MED_COMPOSITION");
                    string effets = dr.GetString("MED_EFFETS");
                    string contreIndic = dr.GetString("MED_CONTREINDIC");
                    float prixEchantillon = dr.GetFloat("MED_PRIXECHANTILLON");
                    medicament = new Medicament(depotLegal, nomCommercial, famCode, famille, composition, effets, contreIndic, prixEchantillon);
                }
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return medicament;
        }

        //Récupérer tous les médicaments
        public List<Medicament> FindAll()
        {
            List<Medicament> medicaments = new List<Medicament>();
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM MEDICAMENT ORDER BY MED_NOMCOMMERCIAL;";
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string depotLegal = dr.GetString("MED_DEPOTLEGAL");
                    string nomCommercial = dr.GetString("MED_NOMCOMMERCIAL");
                    string famCode = dr.GetString("FAM_CODE");
                    FamilleDAO familleDAO = new FamilleDAO();
                    Famille famille = familleDAO.Find(famCode);
                    string composition = dr.GetString("MED_COMPOSITION");
                    string effets = dr.GetString("MED_EFFETS");
                    string contreIndic = dr.GetString("MED_CONTREINDIC");
                    float prixEchantillon = dr.GetFloat("MED_PRIXECHANTILLON");
                    Medicament medicament = new Medicament(depotLegal, nomCommercial, famCode, famille, composition, effets, contreIndic, prixEchantillon);
                    medicaments.Add(medicament);
                }
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return medicaments;
        }

        //Ajouter un médicament
        public void Insert(Medicament medicament)
        {
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "INSERT INTO MEDICAMENT VALUES (@depot, @nomC, @famCode, @compo, @effets, @contreIndic, @prix);";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@depot",medicament.getDepotLegal());
                cmd.Parameters.AddWithValue("@nomC", medicament.getNomCommercial());
                cmd.Parameters.AddWithValue("@famCode", medicament.getCodeFamille());
                cmd.Parameters.AddWithValue("@compo", medicament.getComposition());
                cmd.Parameters.AddWithValue("@effets", medicament.getEffets());
                cmd.Parameters.AddWithValue("@contreIndic", medicament.getContreIndic());
                cmd.Parameters.AddWithValue("@prix", medicament.getPrixEchantillon());
                cmd.ExecuteNonQuery();
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
        }

        //Modifier un médicament
        public void Update(Medicament medicament)
        {
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE MEDICAMENT SET MED_NOMCOMMERCIAL = @nomC, FAM_CODE = @famCode, MED_COMPOSITION = @compo, MED_EFFETS = @effets, MED_CONTREINDIC = @contreIndic, MED_PRIXECHANTILLON = @prix WHERE MED_DEPOTLEGAL = @depot;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@depot", medicament.getDepotLegal());
                cmd.Parameters.AddWithValue("@nomC", medicament.getNomCommercial());
                cmd.Parameters.AddWithValue("@famCode", medicament.getCodeFamille());
                cmd.Parameters.AddWithValue("@compo", medicament.getComposition());
                cmd.Parameters.AddWithValue("@effets", medicament.getEffets());
                cmd.Parameters.AddWithValue("@contreIndic", medicament.getContreIndic());
                cmd.Parameters.AddWithValue("@prix", medicament.getPrixEchantillon());
                cmd.ExecuteNonQuery();
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
        }

        //Supprimer un médicament
        public void Delete(Medicament medicament)
        {
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM MEDICAMENT WHERE MED_DEPOTLEGAL = @depot;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@depot", medicament.getDepotLegal());
                cmd.ExecuteNonQuery();
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
        }
    }
}
