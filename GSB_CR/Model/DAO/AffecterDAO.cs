﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GSB_CR.Model.Object;
using MySql.Data.MySqlClient;

namespace GSB_CR.Model.DAO
{
    class AffecterDAO : AbstractDAO
    {
        //Récupérer les groupes d'un collaborateur
        public List<Groupe> ReadGroups(Collaborateur collaborateur)
        {
            List<Groupe> groupes = new List<Groupe>();
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM AFFECTER INNER JOIN GROUPE ON id = id_groupe WHERE COL_MATRICULE = @matricule;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@matricule", collaborateur.getMatricule());
                MySqlDataReader dr = cmd.ExecuteReader();                
                while (dr.Read())
                {
                    int id = dr.GetInt32("id");
                    string nom = dr.GetString("nom");
                    Groupe groupe = new Groupe(id, nom);
                    groupes.Add(groupe);
                }
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return groupes;
        }

        //Ajout un collaborateur à un ou plusieurs groupes
        public void Insert(Collaborateur collaborateur, List<Groupe> groupes)
        {
            try
            {
                foreach (Groupe g in groupes)
                {
                    Connect();
                    MySqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "INSERT INTO AFFECTER VALUES (@groupe, @mat);";
                    cmd.Prepare();
                    cmd.Parameters.AddWithValue("@groupe", g.getId());
                    cmd.Parameters.AddWithValue("@mat", collaborateur.getMatricule());
                    cmd.ExecuteNonQuery();
                    this.connection.Close();
                }
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
        }
    }
}
