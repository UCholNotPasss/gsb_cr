﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using GSB_CR.Model.Object;

namespace GSB_CR.Model.DAO
{
    class SecteurDAO : AbstractDAO
    {
        //Récupérer un secteur
        public Secteur Find(string code)
        {
            Secteur secteur = null;
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM SECTEUR WHERE SEC_CODE = @code;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@code", code);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string libelle = dr.GetString("SEC_LIBELLE");
                    secteur = new Secteur(code,libelle);
                }
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return secteur;
        }

        //Récupérer tous les secteurs
        public List<Secteur> FindAll()
        {
            List<Secteur> secteurs = new List<Secteur>();
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM SECTEUR;";
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string code = dr.GetString("SEC_CODE");
                    string libelle = dr.GetString("SEC_LIBELLE");
                    secteurs.Add(new Secteur(code, libelle));
                }
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return secteurs;
        }
    }
}
