﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using GSB_CR.Model.Object;

namespace GSB_CR.Model.DAO
{
    class AbstractDAO
    {
        protected MySqlConnection connection;

        //Connexion à la base de données
        protected void Connect()
        {
            string connectionString = "SERVER=192.168.128.2; DATABASE=gsb_cr; UID=gsb; PASSWORD=gsb!2017";
            this.connection = new MySqlConnection(connectionString);
            this.connection.Open();
        }

        //Interrompre la connexion à la base de données
        protected void Disconnect()
        {
            this.connection.Close();
        }
    }
}
