﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GSB_CR.Model.DAO;
using GSB_CR.Model.Object;
using MySql.Data.MySqlClient;

namespace GSB_CR.Model.DAO
{
    class TypePraticienDAO : AbstractDAO
    {
        //Récupérer tous les types de praticien
        public List<TypePraticien> FindAll()
        {
            List<TypePraticien> typePraticiens = new List<TypePraticien>();
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM TYPE_PRATICIEN ORDER BY TYP_LIBELLE;";
                cmd.Prepare();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string code = dr.GetString("TYP_CODE");
                    string libelle = dr.GetString("TYP_LIBELLE");
                    string lieu = dr.GetString("TYP_LIEU");
                    typePraticiens.Add(new TypePraticien(code, libelle, lieu));
                }
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return typePraticiens;
        }

        //Récupérer un praticien
        public TypePraticien Find(string code)
        {
            TypePraticien typePraticien = null;
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM TYPE_PRATICIEN WHERE TYP_CODE = @code;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@code", code);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string libelle = dr.GetString("TYP_LIBELLE");
                    string lieu = dr.GetString("TYP_LIEU");
                    typePraticien = new TypePraticien(code, libelle, lieu);
                }
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return typePraticien;
        }
    }
}
