﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using GSB_CR.Model.Object;

namespace GSB_CR.Model.DAO
{
    class MotifDAO : AbstractDAO
    {
        //Récupérer les motifs
        public List<Motif> FindAll()
        {
            List<Motif> motifs = new List<Motif>();
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM MOTIF ORDER BY LIBELLE;";
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int id = dr.GetInt32("id");
                    string libelle = dr.GetString("libelle");
                    motifs.Add(new Motif(id, libelle));
                }
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return motifs;
        }

        //Récupérer un motif
        public Motif Find(int id)
        {
            Motif motif = null;
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM MOTIF WHERE ID = @id;";
                cmd.Parameters.AddWithValue("@id", id);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string libelle = dr.GetString("libelle");
                    motif = new Motif(id, libelle);
                }
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return motif;
        }

        //Ajouter un motif
        public void Insert(Motif motif)
        {
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "INSERT INTO motif VALUES (NULL, @libelle);";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@libelle", motif.getLibelle());
                cmd.ExecuteNonQuery();
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
        }

        //Récupérer le dernier id inséré
        public int LastInsertId()
        {
            int id = 0;
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT MAX(id) FROM MOTIF;";
                MySqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                    id = dr.GetInt32("MAX(id)");
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return id;
        }
    }
}
