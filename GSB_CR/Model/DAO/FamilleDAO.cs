﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GSB_CR.Model.DAO;
using GSB_CR.Model.Object;
using MySql.Data.MySqlClient;

namespace GSB_CR.Model.DAO
{
    class FamilleDAO : AbstractDAO
    {
        //Récupérer une famille
        public Famille Find(string code)
        {
            Famille famille = null;
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM FAMILLE WHERE FAM_CODE = @code;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@code", code);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string libelle = dr.GetString("FAM_LIBELLE");
                    famille = new Famille(code, libelle);
                }
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return famille;
        }

        //Récupérer toutes les familles
        public List<Famille> FindAll()
        {
            List<Famille> familles = new List<Famille>();
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM FAMILLE;";
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string code = dr.GetString("FAM_CODE");
                    string libelle = dr.GetString("FAM_LIBELLE");
                    familles.Add(new Famille(code, libelle));
                }
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return familles;
        }
    }
}
