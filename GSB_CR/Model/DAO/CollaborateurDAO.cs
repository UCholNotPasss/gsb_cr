﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using GSB_CR.Model.Object;

namespace GSB_CR.Model.DAO
{
    class CollaborateurDAO : AbstractDAO
    {
        //Vérification des informations de connexion
        public Collaborateur Connected(string username, string pwd)
        {
            Collaborateur collaborateur = null;
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM collaborateur WHERE COL_LOGIN = @username AND COL_MOTDEPASSE = @pwd;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@username", username);
                cmd.Parameters.AddWithValue("@pwd", pwd);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string matricule = dr.GetString("COL_MATRICULE");
                    string nom = dr.GetString("COL_NOM");
                    string prenom = dr.GetString("COL_PRENOM");
                    string adresse = dr.GetString("COL_ADRESSE");
                    string cp = dr.GetString("COL_CP");
                    string ville = dr.GetString("COL_VILLE");
                    DateTime dateEmbauche = dr.GetDateTime("COL_DATEEMBAUCHE");
                    string codeSecteur = dr.GetString("SEC_CODE");
                    SecteurDAO secteurDAO = new SecteurDAO();
                    Secteur secteur = secteurDAO.Find(codeSecteur);
                    string codeLaboratoire = dr.GetString("LAB_CODE");
                    LaboratoireDAO laboDAO = new LaboratoireDAO();
                    Laboratoire laboratoire = laboDAO.Find(codeLaboratoire);
                    string login = dr.GetString("COL_LOGIN");
                    string mdp = dr.GetString("COL_MOTDEPASSE");
                    collaborateur = new Collaborateur(matricule, nom, prenom, adresse, cp, ville, dateEmbauche, codeSecteur, secteur, codeLaboratoire, laboratoire, login, mdp);
                }
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return collaborateur;
        }

        //Récupérer tous les collaborateurs
        public List<Collaborateur> FindAll()
        {
            List<Collaborateur> collaborateurs = new List<Collaborateur>();
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM collaborateur ORDER BY COL_NOM, COL_PRENOM;";
                cmd.Prepare();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string matricule = dr.GetString("COL_MATRICULE");
                    string nom = dr.GetString("COL_NOM");
                    string prenom = dr.GetString("COL_PRENOM");
                    string adresse = dr.GetString("COL_ADRESSE");
                    string cp = dr.GetString("COL_CP");
                    string ville = dr.GetString("COL_VILLE");
                    DateTime dateEmbauche = dr.GetDateTime("COL_DATEEMBAUCHE");
                    string codeSecteur = dr.GetString("SEC_CODE");
                    SecteurDAO secteurDAO = new SecteurDAO();
                    Secteur secteur = secteurDAO.Find(codeSecteur);
                    string codeLaboratoire = dr.GetString("LAB_CODE");
                    LaboratoireDAO laboDAO = new LaboratoireDAO();
                    Laboratoire laboratoire = laboDAO.Find(codeLaboratoire);
                    string login = dr.GetString("COL_LOGIN");
                    string mdp = dr.GetString("COL_MOTDEPASSE");
                    collaborateurs.Add(new Collaborateur(matricule, nom, prenom, adresse, cp, ville, dateEmbauche, codeSecteur, secteur, codeLaboratoire, laboratoire, login, mdp));
                }
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return collaborateurs;
        }

        //Récupérer un collaborateur
        public Collaborateur Find(string matricule)
        {
            Collaborateur collaborateur = null;
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM collaborateur WHERE COL_MATRICULE = @matricule;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@matricule", matricule);
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string nom = dr.GetString("COL_NOM");
                    string prenom = dr.GetString("COL_PRENOM");
                    string adresse = dr.GetString("COL_ADRESSE");
                    string cp = dr.GetString("COL_CP");
                    string ville = dr.GetString("COL_VILLE");
                    DateTime dateEmbauche = dr.GetDateTime("COL_DATEEMBAUCHE");
                    string codeSecteur = dr.GetString("SEC_CODE");
                    SecteurDAO secteurDAO = new SecteurDAO();
                    Secteur secteur = secteurDAO.Find(codeSecteur);
                    string codeLaboratoire = dr.GetString("LAB_CODE");
                    LaboratoireDAO laboDAO = new LaboratoireDAO();
                    Laboratoire laboratoire = laboDAO.Find(codeLaboratoire);
                    string login = dr.GetString("COL_LOGIN");
                    string mdp = dr.GetString("COL_MOTDEPASSE");
                    collaborateur = new Collaborateur(matricule, nom, prenom, adresse, cp, ville, dateEmbauche, codeSecteur, secteur, codeLaboratoire, laboratoire, login, mdp);
                }
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
            return collaborateur;
        }

        //Ajouter un collaborateur
        public void Insert(Collaborateur collaborateur)
        {
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "INSERT INTO collaborateur VALUES (@matricule, @nom, @prenom, @adresse, @cp, @ville, @embauche, @login, @mdp, @sec, @labo);";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@matricule", collaborateur.getMatricule());
                cmd.Parameters.AddWithValue("@nom", collaborateur.getNom());
                cmd.Parameters.AddWithValue("@prenom", collaborateur.getPrenom());
                cmd.Parameters.AddWithValue("@adresse", collaborateur.getAdresse());
                cmd.Parameters.AddWithValue("@cp", collaborateur.getCP());
                cmd.Parameters.AddWithValue("@ville", collaborateur.getVille());
                cmd.Parameters.AddWithValue("@embauche", collaborateur.getDateEmbauche());
                cmd.Parameters.AddWithValue("@login", collaborateur.getLogin());
                cmd.Parameters.AddWithValue("@mdp", collaborateur.getMotDePasse());
                cmd.Parameters.AddWithValue("@sec", collaborateur.getCodeSecteur());
                cmd.Parameters.AddWithValue("@labo", collaborateur.getCodeLaboratoire());
                cmd.ExecuteNonQuery();
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
        }

        //Modifier un collaborateur
        public void Update(Collaborateur collaborateur)
        {
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE collaborateur SET COL_ADRESSE = @adresse, COL_CP = @cp, COL_VILLE = @ville, COL_DATEEMBAUCHE = @embauche, COL_LOGIN = @login, COL_MOTDEPASSE = @mdp, SEC_CODE = @sec, LAB_CODE = @labo WHERE COL_MATRICULE = @matricule;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@matricule", collaborateur.getMatricule());
                cmd.Parameters.AddWithValue("@adresse", collaborateur.getAdresse());
                cmd.Parameters.AddWithValue("@cp", collaborateur.getCP());
                cmd.Parameters.AddWithValue("@ville", collaborateur.getVille());
                cmd.Parameters.AddWithValue("@embauche", collaborateur.getDateEmbauche());
                cmd.Parameters.AddWithValue("@login", collaborateur.getLogin());
                cmd.Parameters.AddWithValue("@mdp", collaborateur.getMotDePasse());
                cmd.Parameters.AddWithValue("@sec", collaborateur.getCodeSecteur());
                cmd.Parameters.AddWithValue("@labo", collaborateur.getCodeLaboratoire());
                cmd.ExecuteNonQuery();
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
        }

        //Supprimer un collaborateur
        public void Delete(Collaborateur collaborateur)
        {
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "DELETE FROM collaborateur WHERE COL_MATRICULE = @matricule;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@matricule", collaborateur.getMatricule());
                cmd.ExecuteNonQuery();
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
        }

        //Bloquer un collaborateur
        public void Lock(Collaborateur collaborateur)
        {
            try
            {
                string mdp = collaborateur.getMotDePasse() + "*";
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE collaborateur SET COL_MOTDEPASSE = @mdp WHERE COL_MATRICULE = @matricule;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@matricule", collaborateur.getMatricule());
                cmd.Parameters.AddWithValue("@mdp", mdp);
                cmd.ExecuteNonQuery();
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
        }

        //DéBloquer un collaborateur
        public void Unlock(Collaborateur collaborateur)
        {
            try
            {
                string mdp = collaborateur.getMotDePasse().Substring(0, collaborateur.getMotDePasse().Length - 1);
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "UPDATE collaborateur SET COL_MOTDEPASSE = @mdp WHERE COL_MATRICULE = @matricule;";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@matricule", collaborateur.getMatricule());
                cmd.Parameters.AddWithValue("@mdp", mdp);
                cmd.ExecuteNonQuery();
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
        }
    }
}
