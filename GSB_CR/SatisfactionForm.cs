﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GSB_CR.Model.DAO;
using GSB_CR.Model.Object;
using GSB_CR.View;

namespace GSB_CR
{
    public partial class SatisfactionForm : Form
    {
        string Collaborateur;
        public SatisfactionForm(string collaborateur)
        {
            InitializeComponent();
            Collaborateur = collaborateur;
        }

        /***
         * Remplissage de la liste déroulante des compte-rendus
         **/
        private void SatisfactionForm_Load(object sender, EventArgs e)
        {
            PraticienDAO praticienDAO = new PraticienDAO();
            List<Praticien> praticiens = praticienDAO.FindAll();
            CompteRenduDAO compteRenduDAO = new CompteRenduDAO();
            List<CompteRendu> compteRendus = compteRenduDAO.ReadAllByPraticien(praticiens.Find(x => x.getNom() == "Notini"));
            foreach (CompteRendu cr in compteRendus)
                cboxVisite.Items.Add(cr.getNum() + " / " + cr.getCollaborateur() + " / " + cr.getMotif());
        }

        /***
         * Passer à la page de questions
         **/
        private void btnNext_Click(object sender, EventArgs e)
        {
            QuestionForm questionForm = new QuestionForm(cboxVisite.SelectedText);
            questionForm.Show();
            this.Hide();
        }

        /***
         * Retour au menu
         **/
        private void btnRetour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SatisfactionForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }
    }
}
