﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GSB_CR.Model.Object;
using GSB_CR.Model.DAO;

namespace GSB_CR
{
    public partial class QuestionForm : Form
    {
        /***
         * Propriétés de questions / réponses
         **/
        string CompteRendu;
        const string q1 = "Le visiteur médical est-il courtois ?";
        const string q2 = "Le visiteur médical était-il attentif aux besoins du praticien ?";
        const string q3 = "Recommanderiez-vous GSB à vos confrères ?";
        const string q4 = "Comment évalueriez-vous la qualité de l'entretien avec le visiteur ?";
        int r1, r2, r3, r4;

        private void rad3_CheckedChanged(object sender, EventArgs e)
        {
            refresh();
            rad3.Checked = true;
        }

        private void rad4_CheckedChanged(object sender, EventArgs e)
        {
            refresh();
            rad4.Checked = true;
        }

        private void rad2_CheckedChanged(object sender, EventArgs e)
        {
            refresh();
            rad2.Checked = true;
        }

        /***
         * Passage à la question suivante
         * Ajout de la satisfaction après la dernière question
         **/
        private void btnSuivant_Click(object sender, EventArgs e)
        {
            if (!rad1.Checked && !rad2.Checked && !rad3.Checked && !rad4.Checked && !rad5.Checked)
                MessageBox.Show("Veuillez sélectionner une note");
            else
                switch (lblQuestion.Text)
                {
                    case q1:
                        if (rad1.Checked)
                            r1 = 1;
                        else if (rad2.Checked)
                            r1 = 2;
                        else if (rad3.Checked)
                            r1 = 3;
                        else if (rad4.Checked)
                            r1 = 4;
                        else
                            r1 = 5;
                        break;
                    case q2:
                        if (rad1.Checked)
                            r2 = 1;
                        else if (rad2.Checked)
                            r2 = 2;
                        else if (rad3.Checked)
                            r2 = 3;
                        else if (rad4.Checked)
                            r2 = 4;
                        else
                            r2 = 5;
                        rad2.Visible = false;
                        rad4.Visible = false;
                        break;
                    case q3:
                        if (rad1.Checked)
                            r3 = 1;
                        else if (rad3.Checked)
                            r3 = 3;
                        else
                            r3 = 5;
                        rad2.Visible = true;
                        rad4.Visible = true;
                        btnSuivant.Text = "Enregistrer";
                        break;
                    case q4:
                        if (rad1.Checked)
                            r4 = 1;
                        else if (rad2.Checked)
                            r4 = 2;
                        else if (rad3.Checked)
                            r4 = 3;
                        else if (rad4.Checked)
                            r4 = 4;
                        else
                            r4 = 5;
                        /***
                         * Créer un objet satisfaction et l'insérer dans la base de données
                         **/
                        List<Satisfaction> satisfactions = new List<Satisfaction>();
                        List<int> notes = new List<int>() { r1, r2, r3, r4 };
                        SatisfactionDao satisfactionDao = new SatisfactionDao();
                        foreach(int Note in notes)
                            satisfactions.Add(new Satisfaction(Note, 1, int.Parse(CompteRendu.Split('/')[0]), CompteRendu.Split('/')[1], 1, DateTime.Today));
                        foreach (Satisfaction s in satisfactions)
                            satisfactionDao.Insert(s);
                        break;
                }
        }

        private void rad1_CheckedChanged(object sender, EventArgs e)
        {
            refresh();
            rad1.Checked = true;
        }

        public QuestionForm(string compteRendu)
        {
            InitializeComponent();
            CompteRendu = compteRendu;
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            refresh();
            rad5.Checked = true;
        }

        private void QuestionForm_Load(object sender, EventArgs e)
        {
            lblQuestion.Text = q1;
        }

        private void refresh()
        {
            rad1.Checked = false;
            rad2.Checked = false;
            rad3.Checked = false;
            rad4.Checked = false;
            rad5.Checked = false;
        }
    }
}
