﻿namespace GSB_CR
{
    partial class SatisfactionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SatisfactionForm));
            this.cboxVisite = new System.Windows.Forms.ComboBox();
            this.lblVisite = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnRetour = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cboxVisite
            // 
            this.cboxVisite.FormattingEnabled = true;
            this.cboxVisite.Location = new System.Drawing.Point(140, 9);
            this.cboxVisite.Name = "cboxVisite";
            this.cboxVisite.Size = new System.Drawing.Size(345, 21);
            this.cboxVisite.TabIndex = 0;
            // 
            // lblVisite
            // 
            this.lblVisite.AutoSize = true;
            this.lblVisite.Location = new System.Drawing.Point(12, 12);
            this.lblVisite.Name = "lblVisite";
            this.lblVisite.Size = new System.Drawing.Size(122, 13);
            this.lblVisite.TabIndex = 1;
            this.lblVisite.Text = "Sélectionnez une visite :";
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(62, 52);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(160, 23);
            this.btnNext.TabIndex = 2;
            this.btnNext.Text = "Continuer";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnRetour
            // 
            this.btnRetour.Location = new System.Drawing.Point(260, 52);
            this.btnRetour.Name = "btnRetour";
            this.btnRetour.Size = new System.Drawing.Size(160, 23);
            this.btnRetour.TabIndex = 3;
            this.btnRetour.Text = "Retour";
            this.btnRetour.UseVisualStyleBackColor = true;
            this.btnRetour.Click += new System.EventHandler(this.btnRetour_Click);
            // 
            // SatisfactionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(504, 98);
            this.Controls.Add(this.btnRetour);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.lblVisite);
            this.Controls.Add(this.cboxVisite);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SatisfactionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Formulaire de satisfaction";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SatisfactionForm_FormClosing);
            this.Load += new System.EventHandler(this.SatisfactionForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboxVisite;
        private System.Windows.Forms.Label lblVisite;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnRetour;
    }
}