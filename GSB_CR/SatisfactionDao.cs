﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using GSB_CR.Model.Object;
using GSB_CR.Model.DAO;

namespace GSB_CR
{
    class SatisfactionDao : AbstractDAO
    {
        //Ajouter une satisfaction
        public void Insert(Satisfaction satisfaction)
        {
            try
            {
                Connect();
                MySqlCommand cmd = connection.CreateCommand();
                /***
                 * Méthode d'ajout de satisfaction à modifier !
                 **/
                /*cmd.CommandText = "INSERT INTO satisfaction VALUES (@matricule, @num, @numPra, @date, @dateVisite, @bilan, @idMotif);";
                cmd.Prepare();
                cmd.Parameters.AddWithValue("@matricule", compteRendu.getMatricule());
                cmd.Parameters.AddWithValue("@num", compteRendu.getNum());
                cmd.Parameters.AddWithValue("@numPra", compteRendu.getNumPraticien());
                cmd.Parameters.AddWithValue("@date", compteRendu.getDate());
                cmd.Parameters.AddWithValue("@dateVisite", compteRendu.getDateVisite());
                cmd.Parameters.AddWithValue("@bilan", compteRendu.getBilan());
                cmd.Parameters.AddWithValue("@idMotif", compteRendu.getIdMotif());
                cmd.ExecuteNonQuery();*/
                this.connection.Close();
            }
            catch (MySqlException msex)
            {
                Console.WriteLine("Error : " + msex.Message);
            }
        }
    }
}
