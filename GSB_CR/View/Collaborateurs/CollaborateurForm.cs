﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GSB_CR.Model.DAO;
using GSB_CR.Model.Object;

namespace GSB_CR.View.Collaborateurs
{
    public partial class CollaborateurForm : Form
    {
        //Propriétés
        List<Secteur> secteurs;
        List<Laboratoire> laboratoires;
        List<Collaborateur> collaborateurs;
        List<Groupe> listGroupes, lstGroupesSource, lstGroupesCSource;
        List<string> groupes;
        string Collaborateur, Action;

        public CollaborateurForm(List<string> groupes, string collaborateur, string action)
        {
            InitializeComponent();
            this.groupes = groupes;
            Collaborateur = collaborateur;
            Action = action;
        }

        //Effectuer l'action sur un collaborateur
        private void buttonAction_Click(object sender, EventArgs e)
        {
            switch (Action)
            {
                case "Ajouter":
                    if (lstGroupesCSource.Count != 0 || txtNom.Text == "" || txtPrenom.Text == "" || txtMatricule.Text == "")
                    {
                        string matricule = txtMatricule.Text;
                        string nom = txtNom.Text;
                        string prenom = txtPrenom.Text;
                        string adresse = txtAdresse.Text;
                        string cp = txtCP.Text;
                        string ville = txtVille.Text;
                        DateTime embauche = dtpEmbauche.Value;
                        string login = txtLogin.Text;
                        if (login == "")
                            login = txtNom.Text.ToLower();
                        SHA256 sha256 = SHA256Managed.Create();
                        byte[] bytes = Encoding.UTF8.GetBytes(txtMdp.Text);
                        byte[] hash = sha256.ComputeHash(bytes);
                        string motdepasse = GetStringFromHash(hash).ToLower();
                        Secteur secteur = secteurs.ElementAt(cbxSecteur.SelectedIndex);
                        Laboratoire laboratoire = laboratoires.ElementAt(cbxLabo.SelectedIndex);
                        Collaborateur collab = new Collaborateur(matricule, nom, prenom, adresse, cp, ville, embauche, secteur.getSecCode(), secteur, laboratoire.getLabCode(), laboratoire, login, motdepasse);
                        CollaborateurDAO collabDAO = new CollaborateurDAO();
                        collabDAO.Insert(collab);
                        AffecterDAO affecterDAO = new AffecterDAO();
                        affecterDAO.Insert(collab, lstGroupesCSource);
                        this.Close();
                    }
                    else
                        MessageBox.Show("Veuillez affecter le collaborateur à un groupe ainsi que lui donner un nom, un prénom et un matricule.");
                    break;

                case "Consulter":
                    CollaborateurDAO collaborateurDAO = new CollaborateurDAO();
                    Collaborateur collaborateur = collaborateurs.Find(x => x.getMatricule() == cbxMatricule.SelectedText);
                    if (buttonAction.Text == "Bloquer")
                    {
                        collaborateurDAO.Lock(collaborateur);
                        buttonAction.Text = "Débloquer";
                    }
                    else
                    {
                        collaborateurDAO.Unlock(collaborateur);
                        buttonAction.Text = "Bloquer";
                    }
                    break;

                case "Modifier":
                    if (lstGroupesC.Items.Count != 0)
                    {
                        string matricule = cbxMatricule.Text;
                        string nom = cbxNom.Text.Split(' ')[0];
                        string prenom = cbxNom.Text.Split(' ')[1];
                        string adresse = txtAdresse.Text;
                        string cp = txtCP.Text;
                        string ville = txtVille.Text;
                        DateTime embauche = dtpEmbauche.Value;
                        string login = txtLogin.Text;
                        if (login == "")
                            login = txtNom.Text.ToLower();
                        SHA256 sha256 = SHA256Managed.Create();
                        byte[] bytes = Encoding.UTF8.GetBytes(txtMdp.Text);
                        byte[] hash = sha256.ComputeHash(bytes);
                        string motdepasse = GetStringFromHash(hash).ToLower();
                        Secteur secteur = secteurs.ElementAt(cbxSecteur.SelectedIndex);
                        Laboratoire laboratoire = laboratoires.ElementAt(cbxLabo.SelectedIndex);
                        Collaborateur collab = new Collaborateur(matricule, nom, prenom, adresse, cp, ville, embauche, secteur.getSecCode(), secteur, laboratoire.getLabCode(), laboratoire, login, motdepasse);
                        CollaborateurDAO collabDAO = new CollaborateurDAO();
                        collabDAO.Update(collab);
                        AffecterDAO affecterDAO = new AffecterDAO();
                        List<Groupe> insertGroupes = new List<Groupe>();
                        foreach (Groupe g in lstGroupesCSource)
                            if (listGroupes.FindAll(x => x.getId() == g.getId()).Count == 0)
                                insertGroupes.Add(g);
                        affecterDAO.Insert(collab, insertGroupes);
                        this.Close();
                    }
                    else
                        MessageBox.Show("Veuillez affecter le collaborateur à un groupe.");
                    break;

                default:
                    string mat = cbxMatricule.Text;
                    CollaborateurDAO cDAO = new CollaborateurDAO();
                    cDAO.Delete(cDAO.Find(mat));
                    this.Close();
                    break;
            }
        }

        //Retour au menu de compte-rendu
        private void buttonRetour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Retour au menu de compte-rendu
        private void AjoutCollaborateurForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            MenuCollaborateurForm menuCollaborateurForm = new MenuCollaborateurForm(groupes, Collaborateur);
            menuCollaborateurForm.Show();
        }

        //Récupération de la liste des collaborateurs, des secteurs, des laboratoires
        private void AjoutCollaborateurForm_Load(object sender, EventArgs e)
        {
            SecteurDAO secteurDAO = new SecteurDAO();
            secteurs = secteurDAO.FindAll();
            cbxSecteur.DataSource = secteurs;
            LaboratoireDAO laboratoireDAO = new LaboratoireDAO();
            laboratoires = laboratoireDAO.FindAll();
            cbxLabo.DataSource = laboratoires;
            GroupeDAO groupeDAO = new GroupeDAO();
            listGroupes = groupeDAO.FindAll();
            lstGroupes.DataSource = listGroupes;
            lstGroupesCSource = new List<Groupe>();
            lstGroupesSource = listGroupes;
            CollaborateurDAO collaborateurDAO = new CollaborateurDAO();
            List<string> matricules = new List<string>();
            switch (Action)
            {
                case "Consulter":
                    this.Text = "Consulter un collaborateur";
                    btnAjout.Visible = false;
                    btnAjoutAll.Visible = false;
                    btnSuppr.Visible = false;
                    btnSupprAll.Visible = false;
                    txtMatricule.Visible = false;
                    txtNom.Visible = false;
                    txtPrenom.Visible = false;
                    lblPrenom.Visible = false;
                    cbxMatricule.Visible = true;
                    cbxNom.Visible = true;
                    buttonAction.Text = "Bloquer";
                    txtAdresse.Enabled = false;
                    txtCP.Enabled = false;
                    txtVille.Enabled = false;
                    txtLogin.Enabled = false;
                    txtMdp.Enabled = false;
                    dtpEmbauche.Enabled = false;
                    txtSecteur.Visible = true;
                    txtLabo.Visible = true;
                    collaborateurs = collaborateurDAO.FindAll();
                    foreach (Collaborateur c in collaborateurs)
                        matricules.Add(c.getMatricule());
                    cbxMatricule.DataSource = matricules;
                    cbxNom.DataSource = collaborateurs;
                    break;

                case "Modifier":
                    this.Text = "Modifier un collaborateur";
                    cbxMatricule.Visible = true;
                    cbxNom.Visible = true;
                    txtMatricule.Visible = false;
                    txtNom.Visible = false;
                    lblPrenom.Visible = false;
                    txtPrenom.Visible = false;
                    collaborateurs = collaborateurDAO.FindAll();
                    foreach (Collaborateur c in collaborateurs)
                        matricules.Add(c.getMatricule());
                    cbxMatricule.DataSource = matricules;
                    cbxNom.DataSource = collaborateurs;
                    break;

                case "Supprimer":
                    this.Text = "Supprimer un collaborateur";
                    btnAjout.Visible = false;
                    btnAjoutAll.Visible = false;
                    btnSuppr.Visible = false;
                    btnSupprAll.Visible = false;
                    txtMatricule.Visible = false;
                    txtNom.Visible = false;
                    txtPrenom.Visible = false;
                    lblPrenom.Visible = false;
                    cbxMatricule.Visible = true;
                    cbxNom.Visible = true;
                    buttonAction.Text = "Supprimer";
                    txtAdresse.Enabled = false;
                    txtCP.Enabled = false;
                    txtVille.Enabled = false;
                    txtLogin.Enabled = false;
                    txtMdp.Enabled = false;
                    dtpEmbauche.Enabled = false;
                    txtSecteur.Visible = true;
                    txtLabo.Visible = true;
                    collaborateurs = collaborateurDAO.FindAll();
                    foreach (Collaborateur c in collaborateurs)
                        matricules.Add(c.getMatricule());
                    cbxMatricule.DataSource = matricules;
                    cbxNom.DataSource = collaborateurs;
                    break;
            }
        }

        private string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
                result.Append(hash[i].ToString("X2"));
            return result.ToString();
        }

        //Ajouter tous les groupes dans la liste des groupes du collaborateur
        private void btnAjoutAll_Click(object sender, EventArgs e)
        {
            lstGroupesC.DataSource = listGroupes;
            lstGroupes.DataSource = null;
        }

        //Affichage des informations selon le matricule sélectionné
        private void cbxMatricule_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxNom.DataSource != null)
            {
                cbxNom.SelectedIndex = cbxMatricule.SelectedIndex;
                Collaborateur collaborateur = collaborateurs.ElementAt(cbxMatricule.SelectedIndex);
                BlockAndInfos(collaborateur);
            }
        }

        //Affichage du bouton bloquer, des informations et des groupes dans la listbox
        private void BlockAndInfos(Collaborateur collaborateur)
        {
            txtAdresse.Text = collaborateur.getAdresse();
            txtCP.Text = collaborateur.getCP();
            txtVille.Text = collaborateur.getVille();
            dtpEmbauche.Value = collaborateur.getDateEmbauche();
            txtLogin.Text = collaborateur.getLogin();
            txtMdp.Text = collaborateur.getMotDePasse();
            txtSecteur.Text = collaborateur.getSecteur().getSecLibelle();
            txtLabo.Text = collaborateur.getLaboratoire().getLabNom();
            AffecterDAO affecterDAO = new AffecterDAO();
            if (Action == "Consulter")
            {
                if (collaborateur.getMotDePasse().EndsWith("*"))
                    buttonAction.Text = "Débloquer";
                else
                    buttonAction.Text = "Bloquer";
                lstGroupes.DataSource = affecterDAO.ReadGroups(collaborateur);
            }
            else if (Action == "Modifier")
                lstGroupesC.DataSource = affecterDAO.ReadGroups(collaborateur);
            else if (Action == "Supprimer")
                lstGroupes.DataSource = affecterDAO.ReadGroups(collaborateur);
        }

        //Affichage des informations selon le nom sélectionné
        private void cbxNom_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbxMatricule.SelectedIndex = cbxNom.SelectedIndex;
            Collaborateur collaborateur = collaborateurs.ElementAt(cbxNom.SelectedIndex);
            BlockAndInfos(collaborateur);
        }

        //Ajouter un groupe dans la liste des groupes du collaborateur
        private void btnAjout_Click(object sender, EventArgs e)
        {
            if (lstGroupes.SelectedItem != null)
            {
                Groupe groupe = listGroupes.Find(x => x.getNom() == lstGroupes.SelectedValue.ToString());
                lstGroupesSource = listGroupes.FindAll(x => x != groupe && lstGroupesSource.Contains(x));
                lstGroupesCSource.Add(groupe);
            }
        }

        //Rafraîchir les listes de groupes
        private void RefreshGroups()
        {
            lstGroupes.DataSource = null;
            lstGroupesC.DataSource = null;
            lstGroupes.DataSource = lstGroupesSource;
            lstGroupesC.DataSource = lstGroupesCSource;
            lstGroupes.SelectedItem = null;
            lstGroupesC.SelectedItem = null;
        }

        //Supprimer un groupe de la liste des groupes du collaborateur
        private void btnSuppr_Click(object sender, EventArgs e)
        {
            if (lstGroupesC.SelectedItem != null)
            {
                Groupe groupe = listGroupes.Find(x => x.getNom() == lstGroupesC.SelectedValue.ToString());
                lstGroupesCSource = listGroupes.FindAll(x => x != groupe && lstGroupesCSource.Contains(x));
                lstGroupesSource.Add(groupe);
            }
        }

        //Ajouter tous les groupes de la liste des groupes du collaborateur
        private void btnSupprAll_Click(object sender, EventArgs e)
        {
            lstGroupes.DataSource = listGroupes;
            lstGroupesC.DataSource = null;
        }
    }
}