﻿namespace GSB_CR.View.Collaborateurs
{
    partial class CollaborateurForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CollaborateurForm));
            this.lblMatricule = new System.Windows.Forms.Label();
            this.txtMatricule = new System.Windows.Forms.TextBox();
            this.lblNom = new System.Windows.Forms.Label();
            this.lblPrenom = new System.Windows.Forms.Label();
            this.lblAdresse = new System.Windows.Forms.Label();
            this.lblCP = new System.Windows.Forms.Label();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.txtPrenom = new System.Windows.Forms.TextBox();
            this.txtAdresse = new System.Windows.Forms.TextBox();
            this.txtCP = new System.Windows.Forms.TextBox();
            this.lblVille = new System.Windows.Forms.Label();
            this.txtVille = new System.Windows.Forms.TextBox();
            this.dtpEmbauche = new System.Windows.Forms.DateTimePicker();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblLogin = new System.Windows.Forms.Label();
            this.lblMdp = new System.Windows.Forms.Label();
            this.lblSecteur = new System.Windows.Forms.Label();
            this.lblLabo = new System.Windows.Forms.Label();
            this.txtLogin = new System.Windows.Forms.TextBox();
            this.txtMdp = new System.Windows.Forms.TextBox();
            this.cbxSecteur = new System.Windows.Forms.ComboBox();
            this.cbxLabo = new System.Windows.Forms.ComboBox();
            this.buttonRetour = new System.Windows.Forms.Button();
            this.buttonAction = new System.Windows.Forms.Button();
            this.lblGroupes = new System.Windows.Forms.Label();
            this.lstGroupes = new System.Windows.Forms.ListBox();
            this.lstGroupesC = new System.Windows.Forms.ListBox();
            this.btnAjout = new System.Windows.Forms.Button();
            this.btnSuppr = new System.Windows.Forms.Button();
            this.btnAjoutAll = new System.Windows.Forms.Button();
            this.btnSupprAll = new System.Windows.Forms.Button();
            this.cbxNom = new System.Windows.Forms.ComboBox();
            this.cbxMatricule = new System.Windows.Forms.ComboBox();
            this.txtLabo = new System.Windows.Forms.TextBox();
            this.txtSecteur = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblMatricule
            // 
            this.lblMatricule.AutoSize = true;
            this.lblMatricule.Location = new System.Drawing.Point(23, 24);
            this.lblMatricule.Name = "lblMatricule";
            this.lblMatricule.Size = new System.Drawing.Size(59, 13);
            this.lblMatricule.TabIndex = 1;
            this.lblMatricule.Text = "Matricule : ";
            // 
            // txtMatricule
            // 
            this.txtMatricule.Location = new System.Drawing.Point(88, 21);
            this.txtMatricule.Name = "txtMatricule";
            this.txtMatricule.Size = new System.Drawing.Size(225, 20);
            this.txtMatricule.TabIndex = 2;
            // 
            // lblNom
            // 
            this.lblNom.AutoSize = true;
            this.lblNom.Location = new System.Drawing.Point(23, 65);
            this.lblNom.Name = "lblNom";
            this.lblNom.Size = new System.Drawing.Size(38, 13);
            this.lblNom.TabIndex = 3;
            this.lblNom.Text = "Nom : ";
            // 
            // lblPrenom
            // 
            this.lblPrenom.AutoSize = true;
            this.lblPrenom.Location = new System.Drawing.Point(23, 106);
            this.lblPrenom.Name = "lblPrenom";
            this.lblPrenom.Size = new System.Drawing.Size(52, 13);
            this.lblPrenom.TabIndex = 4;
            this.lblPrenom.Text = "Prénom : ";
            // 
            // lblAdresse
            // 
            this.lblAdresse.AutoSize = true;
            this.lblAdresse.Location = new System.Drawing.Point(23, 149);
            this.lblAdresse.Name = "lblAdresse";
            this.lblAdresse.Size = new System.Drawing.Size(54, 13);
            this.lblAdresse.TabIndex = 5;
            this.lblAdresse.Text = "Adresse : ";
            // 
            // lblCP
            // 
            this.lblCP.AutoSize = true;
            this.lblCP.Location = new System.Drawing.Point(23, 191);
            this.lblCP.Name = "lblCP";
            this.lblCP.Size = new System.Drawing.Size(72, 13);
            this.lblCP.TabIndex = 6;
            this.lblCP.Text = "Code postal : ";
            // 
            // txtNom
            // 
            this.txtNom.Location = new System.Drawing.Point(88, 62);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(225, 20);
            this.txtNom.TabIndex = 7;
            // 
            // txtPrenom
            // 
            this.txtPrenom.Location = new System.Drawing.Point(88, 103);
            this.txtPrenom.Name = "txtPrenom";
            this.txtPrenom.Size = new System.Drawing.Size(225, 20);
            this.txtPrenom.TabIndex = 8;
            // 
            // txtAdresse
            // 
            this.txtAdresse.Location = new System.Drawing.Point(88, 146);
            this.txtAdresse.Name = "txtAdresse";
            this.txtAdresse.Size = new System.Drawing.Size(225, 20);
            this.txtAdresse.TabIndex = 9;
            // 
            // txtCP
            // 
            this.txtCP.Location = new System.Drawing.Point(97, 188);
            this.txtCP.Name = "txtCP";
            this.txtCP.Size = new System.Drawing.Size(62, 20);
            this.txtCP.TabIndex = 10;
            // 
            // lblVille
            // 
            this.lblVille.AutoSize = true;
            this.lblVille.Location = new System.Drawing.Point(165, 191);
            this.lblVille.Name = "lblVille";
            this.lblVille.Size = new System.Drawing.Size(35, 13);
            this.lblVille.TabIndex = 11;
            this.lblVille.Text = "Ville : ";
            // 
            // txtVille
            // 
            this.txtVille.Location = new System.Drawing.Point(206, 188);
            this.txtVille.Name = "txtVille";
            this.txtVille.Size = new System.Drawing.Size(107, 20);
            this.txtVille.TabIndex = 12;
            // 
            // dtpEmbauche
            // 
            this.dtpEmbauche.Location = new System.Drawing.Point(97, 232);
            this.dtpEmbauche.Name = "dtpEmbauche";
            this.dtpEmbauche.Size = new System.Drawing.Size(216, 20);
            this.dtpEmbauche.TabIndex = 13;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(23, 234);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(67, 13);
            this.lblDate.TabIndex = 14;
            this.lblDate.Text = "Embauche : ";
            // 
            // lblLogin
            // 
            this.lblLogin.AutoSize = true;
            this.lblLogin.Location = new System.Drawing.Point(23, 271);
            this.lblLogin.Name = "lblLogin";
            this.lblLogin.Size = new System.Drawing.Size(42, 13);
            this.lblLogin.TabIndex = 15;
            this.lblLogin.Text = "Login : ";
            // 
            // lblMdp
            // 
            this.lblMdp.AutoSize = true;
            this.lblMdp.Location = new System.Drawing.Point(23, 306);
            this.lblMdp.Name = "lblMdp";
            this.lblMdp.Size = new System.Drawing.Size(80, 13);
            this.lblMdp.TabIndex = 16;
            this.lblMdp.Text = "Mot de passe : ";
            // 
            // lblSecteur
            // 
            this.lblSecteur.AutoSize = true;
            this.lblSecteur.Location = new System.Drawing.Point(23, 341);
            this.lblSecteur.Name = "lblSecteur";
            this.lblSecteur.Size = new System.Drawing.Size(53, 13);
            this.lblSecteur.TabIndex = 17;
            this.lblSecteur.Text = "Secteur : ";
            // 
            // lblLabo
            // 
            this.lblLabo.AutoSize = true;
            this.lblLabo.Location = new System.Drawing.Point(23, 378);
            this.lblLabo.Name = "lblLabo";
            this.lblLabo.Size = new System.Drawing.Size(69, 13);
            this.lblLabo.TabIndex = 18;
            this.lblLabo.Text = "Laboratoire : ";
            // 
            // txtLogin
            // 
            this.txtLogin.Location = new System.Drawing.Point(97, 268);
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.Size = new System.Drawing.Size(216, 20);
            this.txtLogin.TabIndex = 19;
            // 
            // txtMdp
            // 
            this.txtMdp.Location = new System.Drawing.Point(97, 303);
            this.txtMdp.Name = "txtMdp";
            this.txtMdp.Size = new System.Drawing.Size(216, 20);
            this.txtMdp.TabIndex = 20;
            // 
            // cbxSecteur
            // 
            this.cbxSecteur.FormattingEnabled = true;
            this.cbxSecteur.Location = new System.Drawing.Point(97, 338);
            this.cbxSecteur.Name = "cbxSecteur";
            this.cbxSecteur.Size = new System.Drawing.Size(216, 21);
            this.cbxSecteur.TabIndex = 21;
            // 
            // cbxLabo
            // 
            this.cbxLabo.FormattingEnabled = true;
            this.cbxLabo.Location = new System.Drawing.Point(97, 375);
            this.cbxLabo.Name = "cbxLabo";
            this.cbxLabo.Size = new System.Drawing.Size(216, 21);
            this.cbxLabo.TabIndex = 22;
            // 
            // buttonRetour
            // 
            this.buttonRetour.Location = new System.Drawing.Point(181, 546);
            this.buttonRetour.Name = "buttonRetour";
            this.buttonRetour.Size = new System.Drawing.Size(132, 23);
            this.buttonRetour.TabIndex = 24;
            this.buttonRetour.Text = "Annuler";
            this.buttonRetour.UseVisualStyleBackColor = true;
            this.buttonRetour.Click += new System.EventHandler(this.buttonRetour_Click);
            // 
            // buttonAction
            // 
            this.buttonAction.Location = new System.Drawing.Point(34, 546);
            this.buttonAction.Name = "buttonAction";
            this.buttonAction.Size = new System.Drawing.Size(125, 23);
            this.buttonAction.TabIndex = 23;
            this.buttonAction.Text = "Enregistrer";
            this.buttonAction.UseVisualStyleBackColor = true;
            this.buttonAction.Click += new System.EventHandler(this.buttonAction_Click);
            // 
            // lblGroupes
            // 
            this.lblGroupes.AutoSize = true;
            this.lblGroupes.Location = new System.Drawing.Point(23, 421);
            this.lblGroupes.Name = "lblGroupes";
            this.lblGroupes.Size = new System.Drawing.Size(53, 13);
            this.lblGroupes.TabIndex = 25;
            this.lblGroupes.Text = "Groupes :";
            // 
            // lstGroupes
            // 
            this.lstGroupes.FormattingEnabled = true;
            this.lstGroupes.Location = new System.Drawing.Point(97, 421);
            this.lstGroupes.Name = "lstGroupes";
            this.lstGroupes.Size = new System.Drawing.Size(89, 108);
            this.lstGroupes.TabIndex = 26;
            // 
            // lstGroupesC
            // 
            this.lstGroupesC.FormattingEnabled = true;
            this.lstGroupesC.Location = new System.Drawing.Point(226, 421);
            this.lstGroupesC.Name = "lstGroupesC";
            this.lstGroupesC.Size = new System.Drawing.Size(87, 108);
            this.lstGroupesC.TabIndex = 27;
            // 
            // btnAjout
            // 
            this.btnAjout.Location = new System.Drawing.Point(192, 449);
            this.btnAjout.Name = "btnAjout";
            this.btnAjout.Size = new System.Drawing.Size(28, 23);
            this.btnAjout.TabIndex = 28;
            this.btnAjout.Text = ">";
            this.btnAjout.UseVisualStyleBackColor = true;
            this.btnAjout.Click += new System.EventHandler(this.btnAjout_Click);
            // 
            // btnSuppr
            // 
            this.btnSuppr.Location = new System.Drawing.Point(192, 478);
            this.btnSuppr.Name = "btnSuppr";
            this.btnSuppr.Size = new System.Drawing.Size(28, 23);
            this.btnSuppr.TabIndex = 29;
            this.btnSuppr.Text = "<";
            this.btnSuppr.UseVisualStyleBackColor = true;
            this.btnSuppr.Click += new System.EventHandler(this.btnSuppr_Click);
            // 
            // btnAjoutAll
            // 
            this.btnAjoutAll.Location = new System.Drawing.Point(192, 421);
            this.btnAjoutAll.Name = "btnAjoutAll";
            this.btnAjoutAll.Size = new System.Drawing.Size(28, 23);
            this.btnAjoutAll.TabIndex = 30;
            this.btnAjoutAll.Text = ">>";
            this.btnAjoutAll.UseVisualStyleBackColor = true;
            this.btnAjoutAll.Click += new System.EventHandler(this.btnAjoutAll_Click);
            // 
            // btnSupprAll
            // 
            this.btnSupprAll.Location = new System.Drawing.Point(192, 506);
            this.btnSupprAll.Name = "btnSupprAll";
            this.btnSupprAll.Size = new System.Drawing.Size(28, 23);
            this.btnSupprAll.TabIndex = 31;
            this.btnSupprAll.Text = "<<";
            this.btnSupprAll.UseVisualStyleBackColor = true;
            this.btnSupprAll.Click += new System.EventHandler(this.btnSupprAll_Click);
            // 
            // cbxNom
            // 
            this.cbxNom.FormattingEnabled = true;
            this.cbxNom.Location = new System.Drawing.Point(89, 62);
            this.cbxNom.Name = "cbxNom";
            this.cbxNom.Size = new System.Drawing.Size(225, 21);
            this.cbxNom.TabIndex = 53;
            this.cbxNom.Visible = false;
            this.cbxNom.SelectedIndexChanged += new System.EventHandler(this.cbxNom_SelectedIndexChanged);
            // 
            // cbxMatricule
            // 
            this.cbxMatricule.FormattingEnabled = true;
            this.cbxMatricule.Location = new System.Drawing.Point(89, 21);
            this.cbxMatricule.Name = "cbxMatricule";
            this.cbxMatricule.Size = new System.Drawing.Size(225, 21);
            this.cbxMatricule.TabIndex = 52;
            this.cbxMatricule.Visible = false;
            this.cbxMatricule.SelectedIndexChanged += new System.EventHandler(this.cbxMatricule_SelectedIndexChanged);
            // 
            // txtLabo
            // 
            this.txtLabo.Enabled = false;
            this.txtLabo.Location = new System.Drawing.Point(98, 375);
            this.txtLabo.Name = "txtLabo";
            this.txtLabo.Size = new System.Drawing.Size(216, 20);
            this.txtLabo.TabIndex = 55;
            this.txtLabo.Visible = false;
            // 
            // txtSecteur
            // 
            this.txtSecteur.Enabled = false;
            this.txtSecteur.Location = new System.Drawing.Point(98, 338);
            this.txtSecteur.Name = "txtSecteur";
            this.txtSecteur.Size = new System.Drawing.Size(216, 20);
            this.txtSecteur.TabIndex = 54;
            this.txtSecteur.Visible = false;
            // 
            // CollaborateurForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(348, 581);
            this.Controls.Add(this.txtLabo);
            this.Controls.Add(this.txtSecteur);
            this.Controls.Add(this.cbxNom);
            this.Controls.Add(this.cbxMatricule);
            this.Controls.Add(this.btnSupprAll);
            this.Controls.Add(this.btnAjoutAll);
            this.Controls.Add(this.btnSuppr);
            this.Controls.Add(this.btnAjout);
            this.Controls.Add(this.lstGroupesC);
            this.Controls.Add(this.lstGroupes);
            this.Controls.Add(this.lblGroupes);
            this.Controls.Add(this.buttonRetour);
            this.Controls.Add(this.buttonAction);
            this.Controls.Add(this.cbxLabo);
            this.Controls.Add(this.cbxSecteur);
            this.Controls.Add(this.txtMdp);
            this.Controls.Add(this.txtLogin);
            this.Controls.Add(this.lblLabo);
            this.Controls.Add(this.lblSecteur);
            this.Controls.Add(this.lblMdp);
            this.Controls.Add(this.lblLogin);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.dtpEmbauche);
            this.Controls.Add(this.txtVille);
            this.Controls.Add(this.lblVille);
            this.Controls.Add(this.txtCP);
            this.Controls.Add(this.txtAdresse);
            this.Controls.Add(this.txtPrenom);
            this.Controls.Add(this.txtNom);
            this.Controls.Add(this.lblCP);
            this.Controls.Add(this.lblAdresse);
            this.Controls.Add(this.lblPrenom);
            this.Controls.Add(this.lblNom);
            this.Controls.Add(this.txtMatricule);
            this.Controls.Add(this.lblMatricule);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CollaborateurForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajouter un collaborateur";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AjoutCollaborateurForm_FormClosing);
            this.Load += new System.EventHandler(this.AjoutCollaborateurForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMatricule;
        private System.Windows.Forms.TextBox txtMatricule;
        private System.Windows.Forms.Label lblNom;
        private System.Windows.Forms.Label lblPrenom;
        private System.Windows.Forms.Label lblAdresse;
        private System.Windows.Forms.Label lblCP;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.TextBox txtPrenom;
        private System.Windows.Forms.TextBox txtAdresse;
        private System.Windows.Forms.TextBox txtCP;
        private System.Windows.Forms.Label lblVille;
        private System.Windows.Forms.TextBox txtVille;
        private System.Windows.Forms.DateTimePicker dtpEmbauche;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblLogin;
        private System.Windows.Forms.Label lblMdp;
        private System.Windows.Forms.Label lblSecteur;
        private System.Windows.Forms.Label lblLabo;
        private System.Windows.Forms.TextBox txtLogin;
        private System.Windows.Forms.TextBox txtMdp;
        private System.Windows.Forms.ComboBox cbxSecteur;
        private System.Windows.Forms.ComboBox cbxLabo;
        private System.Windows.Forms.Button buttonRetour;
        private System.Windows.Forms.Button buttonAction;
        private System.Windows.Forms.Label lblGroupes;
        private System.Windows.Forms.ListBox lstGroupes;
        private System.Windows.Forms.ListBox lstGroupesC;
        private System.Windows.Forms.Button btnAjout;
        private System.Windows.Forms.Button btnSuppr;
        private System.Windows.Forms.Button btnAjoutAll;
        private System.Windows.Forms.Button btnSupprAll;
        private System.Windows.Forms.ComboBox cbxNom;
        private System.Windows.Forms.ComboBox cbxMatricule;
        private System.Windows.Forms.TextBox txtLabo;
        private System.Windows.Forms.TextBox txtSecteur;
    }
}