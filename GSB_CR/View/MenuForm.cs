﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSB_CR.View
{
    public partial class MenuForm : Form
    {
        //Propriétés
        List<string> groupes;
        string Collaborateur;

        public MenuForm(List<string> groupes, string collaborateur)
        {
            InitializeComponent();
            this.groupes = groupes;
            Collaborateur = collaborateur;
        }

        //Ouverture du menu de compte-rendu
        private void buttonCR_Click(object sender, EventArgs e)
        {
            MenuCRForm menuCR = new MenuCRForm(groupes, Collaborateur);
            menuCR.Show();
            this.Hide();
        }

        //Retour à la page de connexion
        private void MenuForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ConnexionForm connexionForm = new ConnexionForm();
            connexionForm.Show();
        }

        //Ouverture du menu de collaborateur
        private void buttonCollaborateur_Click(object sender, EventArgs e)
        {
            MenuCollaborateurForm menuCollaborateurForm = new MenuCollaborateurForm(groupes,Collaborateur);
            menuCollaborateurForm.Show();
            this.Hide();
        }

        //Ouverture du menu de praticien
        private void buttonPraticien_Click(object sender, EventArgs e)
        {
            MenuPraticienForm menuPraticienForm = new MenuPraticienForm(groupes,Collaborateur);
            menuPraticienForm.Show();
            this.Hide();
        }

        //Gestion des droits
        private void MenuForm_Load(object sender, EventArgs e)
        {
            if(groupes.Contains("Visiteur"))
            {
                buttonCollaborateur.Enabled = false;
                buttonMedicament.Enabled = false;
            }
            if(groupes.Contains("Responsable"))
            {
                buttonCollaborateur.Enabled = true;
                buttonMedicament.Enabled = true;
            }
        }

        //Ouverture du menu de médicament
        private void buttonMedicament_Click(object sender, EventArgs e)
        {
            MenuMedicamentForm menuMedicamentForm = new MenuMedicamentForm(groupes, Collaborateur);
            menuMedicamentForm.Show();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SatisfactionForm satisfactionForm = new SatisfactionForm(Collaborateur);
            satisfactionForm.Show();
            this.Hide();
        }
    }
}
