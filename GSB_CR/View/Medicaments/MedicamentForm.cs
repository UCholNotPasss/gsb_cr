﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GSB_CR.Model.DAO;
using GSB_CR.Model.Object;

namespace GSB_CR.View.Medicaments
{
    public partial class MedicamentForm : Form
    {
        //Propriétés
        List<string> groupes;
        string Collaborateur, Action;
        List<Famille> familles;
        List<Medicament> medicaments;

        public MedicamentForm(List<string> groupes, string collaborateur, string action)
        {
            InitializeComponent();
            this.groupes = groupes;
            this.Collaborateur = collaborateur;
            Action = action;
        }

        //Retour au menu de medicament
        private void MedicamentForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            MenuMedicamentForm menuMedicamentForm = new MenuMedicamentForm(groupes, Collaborateur);
            menuMedicamentForm.Show();
        }

        //Récupérer la liste des familles
        private void MedicamentForm_Load(object sender, EventArgs e)
        {
            FamilleDAO familleDAO = new FamilleDAO();
            familles = familleDAO.FindAll();
            cbxFamille.DataSource = familles;
            MedicamentDAO medicamentDAO = new MedicamentDAO();
            medicaments = medicamentDAO.FindAll();
            switch(Action)
            {
                case "Consulter":
                    this.Text = "Consulter un médicament";
                    cbxFamille.Visible = false;
                    txtFamille.Visible = true;
                    numPrix.Visible = false;
                    txtPrix.Visible = true;
                    buttonAction.Visible = false;
                    txtComposition.Enabled = false;
                    txtEffets.Enabled = false;
                    txtContreIndic.Enabled = false;
                    cbxDepot.Visible = true;
                    cbxNom.Visible = true;
                    FeedCbx();
                    break;

                case "Modifier":
                    this.Text = "Modifier un médicament";
                    cbxDepot.Visible = true;
                    cbxNom.Visible = true;
                    FeedCbx();
                    break;

                case "Supprimer":
                    this.Text = "Supprimer un médicament";
                    cbxDepot.Visible = true;
                    cbxNom.Visible = true;
                    cbxFamille.Visible = false;
                    txtFamille.Visible = true;
                    numPrix.Visible = false;
                    txtPrix.Visible = true;
                    txtComposition.Enabled = false;
                    txtEffets.Enabled = false;
                    txtContreIndic.Enabled = false;
                    buttonAction.Text = "Supprimer";
                    FeedCbx();
                    break;
            }
        }

        //Nourrir les listes
        private void FeedCbx()
        {
            List<string> depots = new List<string>();
            List<string> noms = new List<string>();
            foreach(Medicament m in medicaments)
            {
                depots.Add(m.getDepotLegal());
                noms.Add(m.getNomCommercial());
            }
            cbxDepot.DataSource = depots;
            cbxNom.DataSource = noms;
        }

        //Effectuer une action sur un médicament
        private void buttonAction_Click(object sender, EventArgs e)
        {
            MedicamentDAO medicamentDAO = new MedicamentDAO();
            switch (Action)
            {
                case "Ajouter":
                    string depotLegal = txtDepotLegal.Text;
                    string nomCommercial = txtNomCommercial.Text;
                    Famille famille = familles.ElementAt(cbxFamille.SelectedIndex);
                    string composition = txtComposition.Text;
                    string effets = txtEffets.Text;
                    string contreIndic = txtContreIndic.Text;
                    float prixEchantillon = float.Parse(numPrix.Value.ToString());
                    Medicament medicament = new Medicament(depotLegal, nomCommercial, famille.getFamCode(), famille, composition, effets, contreIndic, prixEchantillon);
                    medicamentDAO.Insert(medicament);
                    this.Close();
                    break;

                case "Modifier":
                    string depotLegalM = cbxDepot.Text;
                    string nomCommercialM = cbxNom.Text;
                    Famille familleM = familles.ElementAt(cbxFamille.SelectedIndex);
                    string compositionM = txtComposition.Text;
                    string effetsM = txtEffets.Text;
                    string contreIndicM = txtContreIndic.Text;
                    float prixEchantillonM = float.Parse(numPrix.Value.ToString());
                    Medicament medicamentM = new Medicament(depotLegalM, nomCommercialM, familleM.getFamCode(), familleM, compositionM, effetsM, contreIndicM, prixEchantillonM);
                    medicamentDAO.Update(medicamentM);
                    this.Close();
                    break;

                case "Supprimer":
                    string depotLegalS = cbxDepot.Text;
                    Medicament medicamentS = medicaments.Find(x => x.getDepotLegal() == depotLegalS);
                    medicamentDAO.Delete(medicamentS);
                    this.Close();
                    break;
            }
        }

        //Affichage des informations selon le dépot légal sélectionné
        private void cbxDepot_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbxNom.DataSource != null)
            {
                cbxNom.SelectedIndex = cbxDepot.SelectedIndex;
                Medicament m = medicaments.ElementAt(cbxDepot.SelectedIndex);
                cbxFamille.SelectedIndex = familles.IndexOf(familles.Find(x => x.getFamCode() == m.getCodeFamille()));
                txtFamille.Text = m.getFamille().getFamLibelle();
                txtComposition.Text = m.getComposition();
                txtEffets.Text = m.getEffets();
                txtContreIndic.Text = m.getContreIndic();
                numPrix.Text = m.getPrixEchantillon().ToString();
                txtPrix.Text = m.getPrixEchantillon().ToString();
            }
        }

        //Affichage des informations selon le nom commercial sélectionné
        private void cbxNom_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbxDepot.SelectedIndex = cbxNom.SelectedIndex;
            Medicament m = medicaments.ElementAt(cbxDepot.SelectedIndex);
            cbxFamille.SelectedIndex = familles.IndexOf(familles.Find(x => x.getFamCode() == m.getCodeFamille()));
            txtFamille.Text = m.getFamille().getFamLibelle();
            txtComposition.Text = m.getComposition();
            txtEffets.Text = m.getEffets();
            txtContreIndic.Text = m.getContreIndic();
            numPrix.Text = m.getPrixEchantillon().ToString();
            txtPrix.Text = m.getPrixEchantillon().ToString();
        }

        //Retour au menu de medicament
        private void buttonRetour_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
