﻿namespace GSB_CR.View.Medicaments
{
    partial class MedicamentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MedicamentForm));
            this.txtDepotLegal = new System.Windows.Forms.TextBox();
            this.lblDepotLegal = new System.Windows.Forms.Label();
            this.lblNomCommercial = new System.Windows.Forms.Label();
            this.txtNomCommercial = new System.Windows.Forms.TextBox();
            this.lblFamille = new System.Windows.Forms.Label();
            this.cbxFamille = new System.Windows.Forms.ComboBox();
            this.txtComposition = new System.Windows.Forms.TextBox();
            this.lblComposition = new System.Windows.Forms.Label();
            this.txtEffets = new System.Windows.Forms.TextBox();
            this.lblEffets = new System.Windows.Forms.Label();
            this.txtContreIndic = new System.Windows.Forms.TextBox();
            this.lblContreIndic = new System.Windows.Forms.Label();
            this.lblPrixEchantillon = new System.Windows.Forms.Label();
            this.buttonRetour = new System.Windows.Forms.Button();
            this.buttonAction = new System.Windows.Forms.Button();
            this.numPrix = new System.Windows.Forms.NumericUpDown();
            this.cbxNom = new System.Windows.Forms.ComboBox();
            this.cbxDepot = new System.Windows.Forms.ComboBox();
            this.txtFamille = new System.Windows.Forms.TextBox();
            this.txtPrix = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.numPrix)).BeginInit();
            this.SuspendLayout();
            // 
            // txtDepotLegal
            // 
            this.txtDepotLegal.Location = new System.Drawing.Point(138, 28);
            this.txtDepotLegal.Name = "txtDepotLegal";
            this.txtDepotLegal.Size = new System.Drawing.Size(186, 20);
            this.txtDepotLegal.TabIndex = 4;
            // 
            // lblDepotLegal
            // 
            this.lblDepotLegal.AutoSize = true;
            this.lblDepotLegal.Location = new System.Drawing.Point(29, 31);
            this.lblDepotLegal.Name = "lblDepotLegal";
            this.lblDepotLegal.Size = new System.Drawing.Size(67, 13);
            this.lblDepotLegal.TabIndex = 3;
            this.lblDepotLegal.Text = "Dépot légal :";
            // 
            // lblNomCommercial
            // 
            this.lblNomCommercial.AutoSize = true;
            this.lblNomCommercial.Location = new System.Drawing.Point(29, 69);
            this.lblNomCommercial.Name = "lblNomCommercial";
            this.lblNomCommercial.Size = new System.Drawing.Size(91, 13);
            this.lblNomCommercial.TabIndex = 5;
            this.lblNomCommercial.Text = "Nom commercial :";
            // 
            // txtNomCommercial
            // 
            this.txtNomCommercial.Location = new System.Drawing.Point(138, 66);
            this.txtNomCommercial.Name = "txtNomCommercial";
            this.txtNomCommercial.Size = new System.Drawing.Size(186, 20);
            this.txtNomCommercial.TabIndex = 6;
            // 
            // lblFamille
            // 
            this.lblFamille.AutoSize = true;
            this.lblFamille.Location = new System.Drawing.Point(29, 109);
            this.lblFamille.Name = "lblFamille";
            this.lblFamille.Size = new System.Drawing.Size(45, 13);
            this.lblFamille.TabIndex = 7;
            this.lblFamille.Text = "Famille :";
            // 
            // cbxFamille
            // 
            this.cbxFamille.FormattingEnabled = true;
            this.cbxFamille.Location = new System.Drawing.Point(138, 106);
            this.cbxFamille.Name = "cbxFamille";
            this.cbxFamille.Size = new System.Drawing.Size(186, 21);
            this.cbxFamille.TabIndex = 8;
            // 
            // txtComposition
            // 
            this.txtComposition.Location = new System.Drawing.Point(138, 148);
            this.txtComposition.Multiline = true;
            this.txtComposition.Name = "txtComposition";
            this.txtComposition.Size = new System.Drawing.Size(186, 87);
            this.txtComposition.TabIndex = 10;
            // 
            // lblComposition
            // 
            this.lblComposition.AutoSize = true;
            this.lblComposition.Location = new System.Drawing.Point(29, 151);
            this.lblComposition.Name = "lblComposition";
            this.lblComposition.Size = new System.Drawing.Size(70, 13);
            this.lblComposition.TabIndex = 9;
            this.lblComposition.Text = "Composition :";
            // 
            // txtEffets
            // 
            this.txtEffets.Location = new System.Drawing.Point(138, 258);
            this.txtEffets.Multiline = true;
            this.txtEffets.Name = "txtEffets";
            this.txtEffets.Size = new System.Drawing.Size(186, 87);
            this.txtEffets.TabIndex = 12;
            // 
            // lblEffets
            // 
            this.lblEffets.AutoSize = true;
            this.lblEffets.Location = new System.Drawing.Point(29, 261);
            this.lblEffets.Name = "lblEffets";
            this.lblEffets.Size = new System.Drawing.Size(40, 13);
            this.lblEffets.TabIndex = 11;
            this.lblEffets.Text = "Effets :";
            // 
            // txtContreIndic
            // 
            this.txtContreIndic.Location = new System.Drawing.Point(140, 372);
            this.txtContreIndic.Multiline = true;
            this.txtContreIndic.Name = "txtContreIndic";
            this.txtContreIndic.Size = new System.Drawing.Size(186, 87);
            this.txtContreIndic.TabIndex = 14;
            // 
            // lblContreIndic
            // 
            this.lblContreIndic.AutoSize = true;
            this.lblContreIndic.Location = new System.Drawing.Point(31, 375);
            this.lblContreIndic.Name = "lblContreIndic";
            this.lblContreIndic.Size = new System.Drawing.Size(92, 13);
            this.lblContreIndic.TabIndex = 13;
            this.lblContreIndic.Text = "Contre-indication :";
            // 
            // lblPrixEchantillon
            // 
            this.lblPrixEchantillon.AutoSize = true;
            this.lblPrixEchantillon.Location = new System.Drawing.Point(31, 488);
            this.lblPrixEchantillon.Name = "lblPrixEchantillon";
            this.lblPrixEchantillon.Size = new System.Drawing.Size(103, 13);
            this.lblPrixEchantillon.TabIndex = 15;
            this.lblPrixEchantillon.Text = "Prix de l\'échantillon :";
            // 
            // buttonRetour
            // 
            this.buttonRetour.Location = new System.Drawing.Point(194, 529);
            this.buttonRetour.Name = "buttonRetour";
            this.buttonRetour.Size = new System.Drawing.Size(132, 23);
            this.buttonRetour.TabIndex = 34;
            this.buttonRetour.Text = "Annuler";
            this.buttonRetour.UseVisualStyleBackColor = true;
            this.buttonRetour.Click += new System.EventHandler(this.buttonRetour_Click);
            // 
            // buttonAction
            // 
            this.buttonAction.Location = new System.Drawing.Point(44, 529);
            this.buttonAction.Name = "buttonAction";
            this.buttonAction.Size = new System.Drawing.Size(125, 23);
            this.buttonAction.TabIndex = 33;
            this.buttonAction.Text = "Enregistrer";
            this.buttonAction.UseVisualStyleBackColor = true;
            this.buttonAction.Click += new System.EventHandler(this.buttonAction_Click);
            // 
            // numPrix
            // 
            this.numPrix.DecimalPlaces = 2;
            this.numPrix.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numPrix.Location = new System.Drawing.Point(140, 486);
            this.numPrix.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numPrix.Name = "numPrix";
            this.numPrix.Size = new System.Drawing.Size(186, 20);
            this.numPrix.TabIndex = 35;
            // 
            // cbxNom
            // 
            this.cbxNom.FormattingEnabled = true;
            this.cbxNom.Location = new System.Drawing.Point(138, 66);
            this.cbxNom.Name = "cbxNom";
            this.cbxNom.Size = new System.Drawing.Size(186, 21);
            this.cbxNom.TabIndex = 54;
            this.cbxNom.Visible = false;
            this.cbxNom.SelectedIndexChanged += new System.EventHandler(this.cbxNom_SelectedIndexChanged);
            // 
            // cbxDepot
            // 
            this.cbxDepot.FormattingEnabled = true;
            this.cbxDepot.Location = new System.Drawing.Point(138, 28);
            this.cbxDepot.Name = "cbxDepot";
            this.cbxDepot.Size = new System.Drawing.Size(186, 21);
            this.cbxDepot.TabIndex = 53;
            this.cbxDepot.Visible = false;
            this.cbxDepot.SelectedIndexChanged += new System.EventHandler(this.cbxDepot_SelectedIndexChanged);
            // 
            // txtFamille
            // 
            this.txtFamille.Enabled = false;
            this.txtFamille.Location = new System.Drawing.Point(138, 106);
            this.txtFamille.Name = "txtFamille";
            this.txtFamille.Size = new System.Drawing.Size(186, 20);
            this.txtFamille.TabIndex = 55;
            this.txtFamille.Visible = false;
            // 
            // txtPrix
            // 
            this.txtPrix.Enabled = false;
            this.txtPrix.Location = new System.Drawing.Point(140, 485);
            this.txtPrix.Name = "txtPrix";
            this.txtPrix.Size = new System.Drawing.Size(186, 20);
            this.txtPrix.TabIndex = 56;
            this.txtPrix.Visible = false;
            // 
            // MedicamentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.ClientSize = new System.Drawing.Size(357, 575);
            this.Controls.Add(this.txtPrix);
            this.Controls.Add(this.txtFamille);
            this.Controls.Add(this.cbxNom);
            this.Controls.Add(this.cbxDepot);
            this.Controls.Add(this.numPrix);
            this.Controls.Add(this.buttonRetour);
            this.Controls.Add(this.buttonAction);
            this.Controls.Add(this.lblPrixEchantillon);
            this.Controls.Add(this.txtContreIndic);
            this.Controls.Add(this.lblContreIndic);
            this.Controls.Add(this.txtEffets);
            this.Controls.Add(this.lblEffets);
            this.Controls.Add(this.txtComposition);
            this.Controls.Add(this.lblComposition);
            this.Controls.Add(this.cbxFamille);
            this.Controls.Add(this.lblFamille);
            this.Controls.Add(this.txtNomCommercial);
            this.Controls.Add(this.lblNomCommercial);
            this.Controls.Add(this.txtDepotLegal);
            this.Controls.Add(this.lblDepotLegal);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MedicamentForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajouter un médicament";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MedicamentForm_FormClosing);
            this.Load += new System.EventHandler(this.MedicamentForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numPrix)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtDepotLegal;
        private System.Windows.Forms.Label lblDepotLegal;
        private System.Windows.Forms.Label lblNomCommercial;
        private System.Windows.Forms.TextBox txtNomCommercial;
        private System.Windows.Forms.Label lblFamille;
        private System.Windows.Forms.ComboBox cbxFamille;
        private System.Windows.Forms.TextBox txtComposition;
        private System.Windows.Forms.Label lblComposition;
        private System.Windows.Forms.TextBox txtEffets;
        private System.Windows.Forms.Label lblEffets;
        private System.Windows.Forms.TextBox txtContreIndic;
        private System.Windows.Forms.Label lblContreIndic;
        private System.Windows.Forms.Label lblPrixEchantillon;
        private System.Windows.Forms.Button buttonRetour;
        private System.Windows.Forms.Button buttonAction;
        private System.Windows.Forms.NumericUpDown numPrix;
        private System.Windows.Forms.ComboBox cbxNom;
        private System.Windows.Forms.ComboBox cbxDepot;
        private System.Windows.Forms.TextBox txtFamille;
        private System.Windows.Forms.TextBox txtPrix;
    }
}