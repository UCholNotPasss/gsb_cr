﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GSB_CR.Model.DAO;
using GSB_CR.Model.Object;

namespace GSB_CR.View
{
    public partial class ConnexionForm : Form
    {
        public ConnexionForm()
        {
            InitializeComponent();
        }

        private void buttonConnexion_Click(object sender, EventArgs e)
        {
            //Récupération de l'identifiant
            string utilisateur = textBoxIdentifiant.Text;

            //Récupération et cryptage du mot de passe en SHA256
            SHA256 sha256 = SHA256Managed.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(textBoxMotDePasse.Text);
            byte[] hash = sha256.ComputeHash(bytes);
            string motdepasse = GetStringFromHash(hash).ToLower();
            //string motdepasse = textBoxMotDePasse.Text;

            //Vérification de l'existence de l'utilisateur dans la base
            CollaborateurDAO collaborateurDAO = new CollaborateurDAO();
            Collaborateur collaborateur = collaborateurDAO.Connected(utilisateur, motdepasse);

            //Action résultant de la vérification
            if (collaborateur != null)
            {
                AffecterDAO affecterDAO = new AffecterDAO();
                List<Groupe> groupes = affecterDAO.ReadGroups(collaborateur);
                List<string> noms = new List<string>();
                foreach (Groupe g in groupes)
                    noms.Add(g.getNom());
                MenuForm menu = new MenuForm(noms, utilisateur);
                menu.Show();
                this.Hide();
            }
            else
                MessageBox.Show("Login ou mot de passe incorrect.");
        }

        private string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }

        //Fermeture de l'application
        private void ConnexionForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.ExitThread();
        }
    }
}
