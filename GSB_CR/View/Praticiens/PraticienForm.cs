﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GSB_CR.Model.DAO;
using GSB_CR.Model.Object;

namespace GSB_CR.View.Praticiens
{
    public partial class PraticienForm : Form
    {
        //Propriétés
        List<TypePraticien> typePraticiens;
        List<string> groupes;
        string Collaborateur, Action;
        List<Praticien> praticiens;

        public PraticienForm(List<string> groupes, string collaborateur, string action)
        {
            InitializeComponent();
            this.groupes = groupes;
            this.Collaborateur = collaborateur;
            Action = action;
        }

        //Récupérer la liste des types
        private void PraticienForm_Load(object sender, EventArgs e)
        {
            TypePraticienDAO typePraticienDAO = new TypePraticienDAO();
            typePraticiens = typePraticienDAO.FindAll();
            cbxType.DataSource = typePraticiens;
            cbxNom.Visible = false;
            switch(Action)
            {
                case "Consulter":
                    this.Text = "Consulter un praticien";
                    buttonAction.Visible = false;
                    lblPrenom.Visible = false;
                    txtPrenom.Visible = false;
                    txtNom.Visible = false;
                    txtType.Visible = true;
                    cbxType.Visible = false;
                    numNotoriete.Visible = false;
                    numConfiance.Visible = false;
                    txtNotoriete.Visible = true;
                    txtConfiance.Visible = true;
                    txtAdresse.Enabled = false;
                    txtCP.Enabled = false;
                    txtVille.Enabled = false;
                    FeedCbx();
                    break;

                case "Modifier":
                    this.Text = "Modifier un praticien";
                    lblPrenom.Visible = false;
                    txtPrenom.Visible = false;
                    txtNom.Visible = false;
                    FeedCbx();
                    break;

                case "Supprimer":
                    this.Text = "Supprimer un praticien";
                    buttonAction.Text = "Supprimer";
                    lblPrenom.Visible = false;
                    txtPrenom.Visible = false;
                    txtNom.Visible = false;
                    txtType.Visible = true;
                    cbxType.Visible = false;
                    numNotoriete.Visible = false;
                    numConfiance.Visible = false;
                    txtNotoriete.Visible = true;
                    txtConfiance.Visible = true;
                    txtAdresse.Enabled = false;
                    txtCP.Enabled = false;
                    txtVille.Enabled = false;
                    FeedCbx();
                    break;
            }
        }

        //Nourrir la liste des praticiens
        private void FeedCbx()
        {
            cbxNom.Visible = true;
            PraticienDAO praticienDAO = new PraticienDAO();
            praticiens = praticienDAO.FindAll();
            cbxNom.DataSource = praticiens;
        }

        //Effectuer une action sur un praticien
        private void buttonAction_Click(object sender, EventArgs e)
        {
            string nom = txtNom.Text;
            string prenom = txtPrenom.Text;
            string adresse = txtAdresse.Text;
            string cp = txtCP.Text;
            string ville = txtVille.Text;
            float notoriete = float.Parse(numNotoriete.Value.ToString());
            float confiance = float.Parse(numConfiance.Value.ToString());
            TypePraticien typePraticien = typePraticiens.ElementAt(cbxType.SelectedIndex);
            Praticien praticien = new Praticien(0, nom, prenom, adresse, cp, ville, notoriete, confiance, typePraticien.getCode(), typePraticien);
            PraticienDAO praticienDAO = new PraticienDAO();
            praticienDAO.Insert(praticien);
            this.Close();
        }

        //Retour au menu de praticien
        private void buttonRetour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Afficher les informations selon le praticien sélectionné
        private void cbxNom_SelectedIndexChanged(object sender, EventArgs e)
        {
            Praticien praticien = praticiens.ElementAt(cbxNom.SelectedIndex);
            txtAdresse.Text = praticien.getAdresse();
            txtCP.Text = praticien.getCP();
            txtVille.Text = praticien.getVille();
            numNotoriete.Text = praticien.getCoefNotoriete().ToString();
            txtNotoriete.Text = praticien.getCoefNotoriete().ToString();
            numConfiance.Text = praticien.getCoefConfiance().ToString();
            txtConfiance.Text = praticien.getCoefConfiance().ToString();
            cbxType.SelectedIndex = typePraticiens.IndexOf(typePraticiens.Find(x => x.getCode() == praticien.getTypCode()));
            txtType.Text = praticien.getTypePraticien().getLibelle();
        }

        //Retour au menu de praticien
        private void PraticienForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            MenuPraticienForm menuPraticienForm = new MenuPraticienForm(groupes, Collaborateur);
            menuPraticienForm.Show();
        }
    }
}
