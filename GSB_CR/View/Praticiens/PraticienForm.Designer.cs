﻿namespace GSB_CR.View.Praticiens
{
    partial class PraticienForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PraticienForm));
            this.txtVille = new System.Windows.Forms.TextBox();
            this.lblVille = new System.Windows.Forms.Label();
            this.txtCP = new System.Windows.Forms.TextBox();
            this.txtAdresse = new System.Windows.Forms.TextBox();
            this.txtPrenom = new System.Windows.Forms.TextBox();
            this.txtNom = new System.Windows.Forms.TextBox();
            this.lblCP = new System.Windows.Forms.Label();
            this.lblAdresse = new System.Windows.Forms.Label();
            this.lblPrenom = new System.Windows.Forms.Label();
            this.lblNom = new System.Windows.Forms.Label();
            this.lblNotoriete = new System.Windows.Forms.Label();
            this.lblConfiance = new System.Windows.Forms.Label();
            this.cbxType = new System.Windows.Forms.ComboBox();
            this.lblType = new System.Windows.Forms.Label();
            this.buttonRetour = new System.Windows.Forms.Button();
            this.buttonAction = new System.Windows.Forms.Button();
            this.numNotoriete = new System.Windows.Forms.NumericUpDown();
            this.numConfiance = new System.Windows.Forms.NumericUpDown();
            this.cbxNom = new System.Windows.Forms.ComboBox();
            this.txtNotoriete = new System.Windows.Forms.TextBox();
            this.txtConfiance = new System.Windows.Forms.TextBox();
            this.txtType = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.numNotoriete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numConfiance)).BeginInit();
            this.SuspendLayout();
            // 
            // txtVille
            // 
            this.txtVille.Location = new System.Drawing.Point(209, 154);
            this.txtVille.Name = "txtVille";
            this.txtVille.Size = new System.Drawing.Size(107, 20);
            this.txtVille.TabIndex = 24;
            // 
            // lblVille
            // 
            this.lblVille.AutoSize = true;
            this.lblVille.Location = new System.Drawing.Point(168, 157);
            this.lblVille.Name = "lblVille";
            this.lblVille.Size = new System.Drawing.Size(35, 13);
            this.lblVille.TabIndex = 23;
            this.lblVille.Text = "Ville : ";
            // 
            // txtCP
            // 
            this.txtCP.Location = new System.Drawing.Point(100, 154);
            this.txtCP.Name = "txtCP";
            this.txtCP.Size = new System.Drawing.Size(62, 20);
            this.txtCP.TabIndex = 22;
            // 
            // txtAdresse
            // 
            this.txtAdresse.Location = new System.Drawing.Point(91, 112);
            this.txtAdresse.Name = "txtAdresse";
            this.txtAdresse.Size = new System.Drawing.Size(225, 20);
            this.txtAdresse.TabIndex = 21;
            // 
            // txtPrenom
            // 
            this.txtPrenom.Location = new System.Drawing.Point(91, 69);
            this.txtPrenom.Name = "txtPrenom";
            this.txtPrenom.Size = new System.Drawing.Size(225, 20);
            this.txtPrenom.TabIndex = 20;
            // 
            // txtNom
            // 
            this.txtNom.Location = new System.Drawing.Point(91, 28);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(225, 20);
            this.txtNom.TabIndex = 19;
            // 
            // lblCP
            // 
            this.lblCP.AutoSize = true;
            this.lblCP.Location = new System.Drawing.Point(26, 157);
            this.lblCP.Name = "lblCP";
            this.lblCP.Size = new System.Drawing.Size(72, 13);
            this.lblCP.TabIndex = 18;
            this.lblCP.Text = "Code postal : ";
            // 
            // lblAdresse
            // 
            this.lblAdresse.AutoSize = true;
            this.lblAdresse.Location = new System.Drawing.Point(26, 115);
            this.lblAdresse.Name = "lblAdresse";
            this.lblAdresse.Size = new System.Drawing.Size(54, 13);
            this.lblAdresse.TabIndex = 17;
            this.lblAdresse.Text = "Adresse : ";
            // 
            // lblPrenom
            // 
            this.lblPrenom.AutoSize = true;
            this.lblPrenom.Location = new System.Drawing.Point(26, 72);
            this.lblPrenom.Name = "lblPrenom";
            this.lblPrenom.Size = new System.Drawing.Size(52, 13);
            this.lblPrenom.TabIndex = 16;
            this.lblPrenom.Text = "Prénom : ";
            // 
            // lblNom
            // 
            this.lblNom.AutoSize = true;
            this.lblNom.Location = new System.Drawing.Point(26, 31);
            this.lblNom.Name = "lblNom";
            this.lblNom.Size = new System.Drawing.Size(38, 13);
            this.lblNom.TabIndex = 15;
            this.lblNom.Text = "Nom : ";
            // 
            // lblNotoriete
            // 
            this.lblNotoriete.AutoSize = true;
            this.lblNotoriete.Location = new System.Drawing.Point(26, 199);
            this.lblNotoriete.Name = "lblNotoriete";
            this.lblNotoriete.Size = new System.Drawing.Size(122, 13);
            this.lblNotoriete.TabIndex = 25;
            this.lblNotoriete.Text = "Coeficient de notoriété : ";
            // 
            // lblConfiance
            // 
            this.lblConfiance.AutoSize = true;
            this.lblConfiance.Location = new System.Drawing.Point(26, 236);
            this.lblConfiance.Name = "lblConfiance";
            this.lblConfiance.Size = new System.Drawing.Size(128, 13);
            this.lblConfiance.TabIndex = 27;
            this.lblConfiance.Text = "Coeficient de confiance : ";
            // 
            // cbxType
            // 
            this.cbxType.FormattingEnabled = true;
            this.cbxType.Location = new System.Drawing.Point(100, 271);
            this.cbxType.Name = "cbxType";
            this.cbxType.Size = new System.Drawing.Size(216, 21);
            this.cbxType.TabIndex = 29;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(26, 274);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(40, 13);
            this.lblType.TabIndex = 30;
            this.lblType.Text = "Type : ";
            // 
            // buttonRetour
            // 
            this.buttonRetour.Location = new System.Drawing.Point(184, 323);
            this.buttonRetour.Name = "buttonRetour";
            this.buttonRetour.Size = new System.Drawing.Size(132, 23);
            this.buttonRetour.TabIndex = 32;
            this.buttonRetour.Text = "Annuler";
            this.buttonRetour.UseVisualStyleBackColor = true;
            this.buttonRetour.Click += new System.EventHandler(this.buttonRetour_Click);
            // 
            // buttonAction
            // 
            this.buttonAction.Location = new System.Drawing.Point(37, 323);
            this.buttonAction.Name = "buttonAction";
            this.buttonAction.Size = new System.Drawing.Size(125, 23);
            this.buttonAction.TabIndex = 31;
            this.buttonAction.Text = "Enregistrer";
            this.buttonAction.UseVisualStyleBackColor = true;
            this.buttonAction.Click += new System.EventHandler(this.buttonAction_Click);
            // 
            // numNotoriete
            // 
            this.numNotoriete.DecimalPlaces = 2;
            this.numNotoriete.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numNotoriete.Location = new System.Drawing.Point(171, 197);
            this.numNotoriete.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numNotoriete.Name = "numNotoriete";
            this.numNotoriete.Size = new System.Drawing.Size(145, 20);
            this.numNotoriete.TabIndex = 33;
            // 
            // numConfiance
            // 
            this.numConfiance.DecimalPlaces = 2;
            this.numConfiance.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numConfiance.Location = new System.Drawing.Point(171, 234);
            this.numConfiance.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numConfiance.Name = "numConfiance";
            this.numConfiance.Size = new System.Drawing.Size(145, 20);
            this.numConfiance.TabIndex = 34;
            // 
            // cbxNom
            // 
            this.cbxNom.FormattingEnabled = true;
            this.cbxNom.Location = new System.Drawing.Point(91, 28);
            this.cbxNom.Name = "cbxNom";
            this.cbxNom.Size = new System.Drawing.Size(225, 21);
            this.cbxNom.TabIndex = 52;
            this.cbxNom.SelectedIndexChanged += new System.EventHandler(this.cbxNom_SelectedIndexChanged);
            // 
            // txtNotoriete
            // 
            this.txtNotoriete.Enabled = false;
            this.txtNotoriete.Location = new System.Drawing.Point(171, 196);
            this.txtNotoriete.Name = "txtNotoriete";
            this.txtNotoriete.Size = new System.Drawing.Size(145, 20);
            this.txtNotoriete.TabIndex = 53;
            this.txtNotoriete.Visible = false;
            // 
            // txtConfiance
            // 
            this.txtConfiance.Enabled = false;
            this.txtConfiance.Location = new System.Drawing.Point(171, 233);
            this.txtConfiance.Name = "txtConfiance";
            this.txtConfiance.Size = new System.Drawing.Size(145, 20);
            this.txtConfiance.TabIndex = 54;
            this.txtConfiance.Visible = false;
            // 
            // txtType
            // 
            this.txtType.Enabled = false;
            this.txtType.Location = new System.Drawing.Point(100, 271);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(216, 20);
            this.txtType.TabIndex = 55;
            this.txtType.Visible = false;
            // 
            // PraticienForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(348, 377);
            this.Controls.Add(this.txtType);
            this.Controls.Add(this.txtConfiance);
            this.Controls.Add(this.txtNotoriete);
            this.Controls.Add(this.cbxNom);
            this.Controls.Add(this.numConfiance);
            this.Controls.Add(this.numNotoriete);
            this.Controls.Add(this.buttonRetour);
            this.Controls.Add(this.buttonAction);
            this.Controls.Add(this.lblType);
            this.Controls.Add(this.cbxType);
            this.Controls.Add(this.lblConfiance);
            this.Controls.Add(this.lblNotoriete);
            this.Controls.Add(this.txtVille);
            this.Controls.Add(this.lblVille);
            this.Controls.Add(this.txtCP);
            this.Controls.Add(this.txtAdresse);
            this.Controls.Add(this.txtPrenom);
            this.Controls.Add(this.txtNom);
            this.Controls.Add(this.lblCP);
            this.Controls.Add(this.lblAdresse);
            this.Controls.Add(this.lblPrenom);
            this.Controls.Add(this.lblNom);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PraticienForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajouter un praticien";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PraticienForm_FormClosing);
            this.Load += new System.EventHandler(this.PraticienForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numNotoriete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numConfiance)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtVille;
        private System.Windows.Forms.Label lblVille;
        private System.Windows.Forms.TextBox txtCP;
        private System.Windows.Forms.TextBox txtAdresse;
        private System.Windows.Forms.TextBox txtPrenom;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.Label lblCP;
        private System.Windows.Forms.Label lblAdresse;
        private System.Windows.Forms.Label lblPrenom;
        private System.Windows.Forms.Label lblNom;
        private System.Windows.Forms.Label lblNotoriete;
        private System.Windows.Forms.Label lblConfiance;
        private System.Windows.Forms.ComboBox cbxType;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.Button buttonRetour;
        private System.Windows.Forms.Button buttonAction;
        private System.Windows.Forms.NumericUpDown numNotoriete;
        private System.Windows.Forms.NumericUpDown numConfiance;
        private System.Windows.Forms.ComboBox cbxNom;
        private System.Windows.Forms.TextBox txtNotoriete;
        private System.Windows.Forms.TextBox txtConfiance;
        private System.Windows.Forms.TextBox txtType;
    }
}