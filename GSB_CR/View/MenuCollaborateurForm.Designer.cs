﻿namespace GSB_CR.View
{
    partial class MenuCollaborateurForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuCollaborateurForm));
            this.buttonConsulterCollaborateur = new System.Windows.Forms.Button();
            this.buttonAjouterCollaborateur = new System.Windows.Forms.Button();
            this.buttonModifier = new System.Windows.Forms.Button();
            this.buttonQuitter = new System.Windows.Forms.Button();
            this.buttonSupprimer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonConsulterCollaborateur
            // 
            this.buttonConsulterCollaborateur.Image = ((System.Drawing.Image)(resources.GetObject("buttonConsulterCollaborateur.Image")));
            this.buttonConsulterCollaborateur.Location = new System.Drawing.Point(12, 12);
            this.buttonConsulterCollaborateur.Name = "buttonConsulterCollaborateur";
            this.buttonConsulterCollaborateur.Size = new System.Drawing.Size(90, 90);
            this.buttonConsulterCollaborateur.TabIndex = 1;
            this.buttonConsulterCollaborateur.UseVisualStyleBackColor = true;
            this.buttonConsulterCollaborateur.Click += new System.EventHandler(this.buttonConsulterCollaborateur_Click);
            // 
            // buttonAjouterCollaborateur
            // 
            this.buttonAjouterCollaborateur.Image = ((System.Drawing.Image)(resources.GetObject("buttonAjouterCollaborateur.Image")));
            this.buttonAjouterCollaborateur.Location = new System.Drawing.Point(108, 12);
            this.buttonAjouterCollaborateur.Name = "buttonAjouterCollaborateur";
            this.buttonAjouterCollaborateur.Size = new System.Drawing.Size(90, 90);
            this.buttonAjouterCollaborateur.TabIndex = 2;
            this.buttonAjouterCollaborateur.UseVisualStyleBackColor = true;
            this.buttonAjouterCollaborateur.Click += new System.EventHandler(this.buttonAjouterCollaborateur_Click);
            // 
            // buttonModifier
            // 
            this.buttonModifier.Image = ((System.Drawing.Image)(resources.GetObject("buttonModifier.Image")));
            this.buttonModifier.Location = new System.Drawing.Point(204, 12);
            this.buttonModifier.Name = "buttonModifier";
            this.buttonModifier.Size = new System.Drawing.Size(90, 90);
            this.buttonModifier.TabIndex = 3;
            this.buttonModifier.UseVisualStyleBackColor = true;
            this.buttonModifier.Click += new System.EventHandler(this.buttonModifier_Click);
            // 
            // buttonQuitter
            // 
            this.buttonQuitter.Image = ((System.Drawing.Image)(resources.GetObject("buttonQuitter.Image")));
            this.buttonQuitter.Location = new System.Drawing.Point(396, 12);
            this.buttonQuitter.Name = "buttonQuitter";
            this.buttonQuitter.Size = new System.Drawing.Size(90, 90);
            this.buttonQuitter.TabIndex = 4;
            this.buttonQuitter.UseVisualStyleBackColor = true;
            this.buttonQuitter.Click += new System.EventHandler(this.buttonQuitter_Click);
            // 
            // buttonSupprimer
            // 
            this.buttonSupprimer.Image = ((System.Drawing.Image)(resources.GetObject("buttonSupprimer.Image")));
            this.buttonSupprimer.Location = new System.Drawing.Point(300, 12);
            this.buttonSupprimer.Name = "buttonSupprimer";
            this.buttonSupprimer.Size = new System.Drawing.Size(90, 90);
            this.buttonSupprimer.TabIndex = 5;
            this.buttonSupprimer.UseVisualStyleBackColor = true;
            this.buttonSupprimer.Click += new System.EventHandler(this.buttonSupprimer_Click);
            // 
            // MenuCollaborateurForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.ClientSize = new System.Drawing.Size(502, 115);
            this.Controls.Add(this.buttonSupprimer);
            this.Controls.Add(this.buttonQuitter);
            this.Controls.Add(this.buttonModifier);
            this.Controls.Add(this.buttonAjouterCollaborateur);
            this.Controls.Add(this.buttonConsulterCollaborateur);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MenuCollaborateurForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Collaborateur : Menu";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MenuCollaborateurForm_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonConsulterCollaborateur;
        private System.Windows.Forms.Button buttonAjouterCollaborateur;
        private System.Windows.Forms.Button buttonModifier;
        private System.Windows.Forms.Button buttonQuitter;
        private System.Windows.Forms.Button buttonSupprimer;
    }
}