﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSB_CR.View
{
    public partial class MenuPraticienForm : Form
    {
        //Propriétés
        List<string> groupes;
        string Collaborateur;

        public MenuPraticienForm(List<string> groupes, string collaborateur)
        {
            InitializeComponent();
            this.groupes = groupes;
            this.Collaborateur = collaborateur;
        }

        //Retour à la boite à outils
        private void MenuPraticienForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            MenuForm menuForm = new MenuForm(groupes, Collaborateur);
            menuForm.Show();
        }

        //Affichage de la page de consultation des praticiens
        private void buttonConsulterPraticien_Click(object sender, EventArgs e)
        {
            Praticiens.PraticienForm praticienForm = new Praticiens.PraticienForm(groupes, Collaborateur, "Consulter");
            praticienForm.Show();
            this.Hide();
        }

        //Affichage de la page d'ajout des praticiens
        private void buttonAjouterPraticien_Click(object sender, EventArgs e)
        {
            Praticiens.PraticienForm praticienForm = new Praticiens.PraticienForm(groupes, Collaborateur, "Ajouter");
            praticienForm.Show();
            this.Hide();
        }

        //Affichage de la page de modification des praticiens
        private void buttonModifierPraticien_Click(object sender, EventArgs e)
        {
            Praticiens.PraticienForm praticienForm = new Praticiens.PraticienForm(groupes, Collaborateur, "Modifier");
            praticienForm.Show();
            this.Hide();
        }


        //Affichage de la page de suppression des praticiens
        private void buttonSupprimerPraticien_Click(object sender, EventArgs e)
        {
            Praticiens.PraticienForm praticienForm = new Praticiens.PraticienForm(groupes, Collaborateur, "Supprimer");
            praticienForm.Show();
            this.Hide();
        }

        //Retour à la boite à outils
        private void buttonRetour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Gestion des droits
        private void MenuPraticienForm_Load(object sender, EventArgs e)
        {
            if(groupes.Contains("Visiteur"))
            {
                buttonSupprimerPraticien.Enabled = false;
            }
            if(groupes.Contains("Responsable"))
            {
                buttonSupprimerPraticien.Enabled = true;
            }
        }
    }
}
