﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSB_CR.View
{
    public partial class MenuMedicamentForm : Form
    {
        //Propriétés
        List<string> groupes;
        string Collaborateur;

        public MenuMedicamentForm(List<string> groupes, string collaborateur)
        {
            InitializeComponent();
            this.groupes = groupes;
            this.Collaborateur = collaborateur;
        }

        //Retour à la boite à outils
        private void MenuMedicamentForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            MenuForm menuForm = new MenuForm(groupes, Collaborateur);
            menuForm.Show();
        }

        //Retour à la boite à outils
        private void buttonQuitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Affichage de la page de consultation de médicaments
        private void buttonConsulterMed_Click(object sender, EventArgs e)
        {
            Medicaments.MedicamentForm MedicamentForm = new Medicaments.MedicamentForm(groupes, Collaborateur, "Consulter");
            MedicamentForm.Show();
            this.Hide();
        }
        
        //Affichage de la page d'ajout de médicaments
        private void buttonAjouterMed_Click(object sender, EventArgs e)
        {
            Medicaments.MedicamentForm MedicamentForm = new Medicaments.MedicamentForm(groupes, Collaborateur, "Ajouter");
            MedicamentForm.Show();
            this.Hide();
        }

        //Affichage de la page de modification de médicaments
        private void buttonModifierMed_Click(object sender, EventArgs e)
        {
            Medicaments.MedicamentForm MedicamentForm = new Medicaments.MedicamentForm(groupes, Collaborateur, "Modifier");
            MedicamentForm.Show();
            this.Hide();
        }

        //Affichage de la page de suppression de médicaments
        private void buttonSupprimer_Click(object sender, EventArgs e)
        {
            Medicaments.MedicamentForm MedicamentForm = new Medicaments.MedicamentForm(groupes, Collaborateur, "Supprimer");
            MedicamentForm.Show();
            this.Hide();
        }
    }
}
