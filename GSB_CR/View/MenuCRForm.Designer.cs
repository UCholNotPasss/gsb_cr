﻿namespace GSB_CR.View
{
    partial class MenuCRForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuCRForm));
            this.buttonConsulterCR = new System.Windows.Forms.Button();
            this.buttonAjouterCR = new System.Windows.Forms.Button();
            this.buttonModifierCR = new System.Windows.Forms.Button();
            this.buttonQuitter = new System.Windows.Forms.Button();
            this.buttonSupprimer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonConsulterCR
            // 
            this.buttonConsulterCR.Image = ((System.Drawing.Image)(resources.GetObject("buttonConsulterCR.Image")));
            this.buttonConsulterCR.Location = new System.Drawing.Point(12, 12);
            this.buttonConsulterCR.Name = "buttonConsulterCR";
            this.buttonConsulterCR.Size = new System.Drawing.Size(90, 90);
            this.buttonConsulterCR.TabIndex = 0;
            this.buttonConsulterCR.UseVisualStyleBackColor = true;
            this.buttonConsulterCR.Click += new System.EventHandler(this.buttonConsulterCR_Click);
            // 
            // buttonAjouterCR
            // 
            this.buttonAjouterCR.Image = ((System.Drawing.Image)(resources.GetObject("buttonAjouterCR.Image")));
            this.buttonAjouterCR.Location = new System.Drawing.Point(108, 12);
            this.buttonAjouterCR.Name = "buttonAjouterCR";
            this.buttonAjouterCR.Size = new System.Drawing.Size(90, 90);
            this.buttonAjouterCR.TabIndex = 1;
            this.buttonAjouterCR.UseVisualStyleBackColor = true;
            this.buttonAjouterCR.Click += new System.EventHandler(this.buttonAjouterCR_Click);
            // 
            // buttonModifierCR
            // 
            this.buttonModifierCR.Image = ((System.Drawing.Image)(resources.GetObject("buttonModifierCR.Image")));
            this.buttonModifierCR.Location = new System.Drawing.Point(204, 12);
            this.buttonModifierCR.Name = "buttonModifierCR";
            this.buttonModifierCR.Size = new System.Drawing.Size(90, 90);
            this.buttonModifierCR.TabIndex = 2;
            this.buttonModifierCR.UseVisualStyleBackColor = true;
            this.buttonModifierCR.Click += new System.EventHandler(this.buttonModifierCR_Click);
            // 
            // buttonQuitter
            // 
            this.buttonQuitter.Image = ((System.Drawing.Image)(resources.GetObject("buttonQuitter.Image")));
            this.buttonQuitter.Location = new System.Drawing.Point(396, 12);
            this.buttonQuitter.Name = "buttonQuitter";
            this.buttonQuitter.Size = new System.Drawing.Size(90, 90);
            this.buttonQuitter.TabIndex = 3;
            this.buttonQuitter.UseVisualStyleBackColor = true;
            this.buttonQuitter.Click += new System.EventHandler(this.buttonQuitter_Click);
            // 
            // buttonSupprimer
            // 
            this.buttonSupprimer.Image = ((System.Drawing.Image)(resources.GetObject("buttonSupprimer.Image")));
            this.buttonSupprimer.Location = new System.Drawing.Point(300, 12);
            this.buttonSupprimer.Name = "buttonSupprimer";
            this.buttonSupprimer.Size = new System.Drawing.Size(90, 90);
            this.buttonSupprimer.TabIndex = 6;
            this.buttonSupprimer.UseVisualStyleBackColor = true;
            this.buttonSupprimer.Click += new System.EventHandler(this.buttonSupprimer_Click);
            // 
            // MenuCRForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(500, 115);
            this.Controls.Add(this.buttonSupprimer);
            this.Controls.Add(this.buttonQuitter);
            this.Controls.Add(this.buttonModifierCR);
            this.Controls.Add(this.buttonAjouterCR);
            this.Controls.Add(this.buttonConsulterCR);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MenuCRForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Compte-Rendu : Menu";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MenuCRForm_FormClosing);
            this.Load += new System.EventHandler(this.MenuCRForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonConsulterCR;
        private System.Windows.Forms.Button buttonAjouterCR;
        private System.Windows.Forms.Button buttonModifierCR;
        private System.Windows.Forms.Button buttonQuitter;
        private System.Windows.Forms.Button buttonSupprimer;
    }
}