﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSB_CR.View
{
    public partial class MenuCRForm : Form
    {
        //Propriétés
        List<string> groupes;
        string Collaborateur;

        public MenuCRForm(List<string> groupes, string collaborateur)
        {
            InitializeComponent();
            this.groupes = groupes;
            Collaborateur = collaborateur;
        }

        //Affichage de la page de consultation des compte-rendus
        private void buttonConsulterCR_Click(object sender, EventArgs e)
        {
            CR.CRForm cRForm = new CR.CRForm(groupes, Collaborateur, "Consulter");
            cRForm.Show();
            this.Hide();
        }

        //Affichage de la page d'ajout d'un compte-rendu
        private void buttonAjouterCR_Click(object sender, EventArgs e)
        {
            CR.CRForm cRForm = new CR.CRForm(groupes, Collaborateur, "Ajouter");
            cRForm.Show();
            this.Hide();
        }

        //Affichage de la page de modification des compte-rendus
        private void buttonModifierCR_Click(object sender, EventArgs e)
        {
            CR.CRForm cRForm = new CR.CRForm(groupes, Collaborateur, "Modifier");
            cRForm.Show();
            this.Hide();
        }

        //Retour à la boite à outils
        private void buttonQuitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Retour à la boite à outils
        private void MenuCRForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            MenuForm menu = new MenuForm(groupes, Collaborateur);
            menu.Show();
        }

        //Gestion des droits
        private void MenuCRForm_Load(object sender, EventArgs e)
        {
            if(groupes.Contains("Visiteur"))
            {
                buttonAjouterCR.Enabled = true;
                buttonConsulterCR.Enabled = true;
                buttonModifierCR.Enabled = true;
                buttonSupprimer.Enabled = true;
            }
            if(groupes.Contains("Responsable"))
            {
                buttonAjouterCR.Enabled = false;
                buttonConsulterCR.Enabled = true;
                buttonModifierCR.Enabled = false;
                buttonSupprimer.Enabled = false;
            }
        }

        //Affichage de la page de suppression des compte-rendus
        private void buttonSupprimer_Click(object sender, EventArgs e)
        {
            CR.CRForm cRForm = new CR.CRForm(groupes, Collaborateur, "Supprimer");
            cRForm.Show();
            this.Hide();
        }
    }
}
