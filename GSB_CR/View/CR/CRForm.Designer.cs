﻿namespace GSB_CR.View.CR
{
    partial class CRForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CRForm));
            this.lblCollaborateur = new System.Windows.Forms.Label();
            this.cboxCollaborateur = new System.Windows.Forms.ComboBox();
            this.lblPraticien = new System.Windows.Forms.Label();
            this.cboxPraticien = new System.Windows.Forms.ComboBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.chboxDate = new System.Windows.Forms.CheckBox();
            this.lblDateVisite = new System.Windows.Forms.Label();
            this.dtpDateVisite = new System.Windows.Forms.DateTimePicker();
            this.lblBilan = new System.Windows.Forms.Label();
            this.txtBilan = new System.Windows.Forms.TextBox();
            this.lblMotif = new System.Windows.Forms.Label();
            this.cboxMotif = new System.Windows.Forms.ComboBox();
            this.buttonAction = new System.Windows.Forms.Button();
            this.buttonRetour = new System.Windows.Forms.Button();
            this.txtAutreMotif = new System.Windows.Forms.TextBox();
            this.chboxAutreMotif = new System.Windows.Forms.CheckBox();
            this.txtPraticien = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblCollaborateur
            // 
            this.lblCollaborateur.AutoSize = true;
            this.lblCollaborateur.Location = new System.Drawing.Point(12, 16);
            this.lblCollaborateur.Name = "lblCollaborateur";
            this.lblCollaborateur.Size = new System.Drawing.Size(78, 13);
            this.lblCollaborateur.TabIndex = 0;
            this.lblCollaborateur.Text = "Collaborateur : ";
            // 
            // cboxCollaborateur
            // 
            this.cboxCollaborateur.FormattingEnabled = true;
            this.cboxCollaborateur.Location = new System.Drawing.Point(112, 13);
            this.cboxCollaborateur.Name = "cboxCollaborateur";
            this.cboxCollaborateur.Size = new System.Drawing.Size(200, 21);
            this.cboxCollaborateur.TabIndex = 1;
            this.cboxCollaborateur.SelectedIndexChanged += new System.EventHandler(this.cboxCollaborateur_SelectedIndexChanged);
            // 
            // lblPraticien
            // 
            this.lblPraticien.AutoSize = true;
            this.lblPraticien.Location = new System.Drawing.Point(13, 61);
            this.lblPraticien.Name = "lblPraticien";
            this.lblPraticien.Size = new System.Drawing.Size(57, 13);
            this.lblPraticien.TabIndex = 2;
            this.lblPraticien.Text = "Praticien : ";
            // 
            // cboxPraticien
            // 
            this.cboxPraticien.FormattingEnabled = true;
            this.cboxPraticien.Location = new System.Drawing.Point(112, 58);
            this.cboxPraticien.Name = "cboxPraticien";
            this.cboxPraticien.Size = new System.Drawing.Size(200, 21);
            this.cboxPraticien.TabIndex = 3;
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(13, 111);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(39, 13);
            this.lblDate.TabIndex = 4;
            this.lblDate.Text = "Date : ";
            // 
            // dtpDate
            // 
            this.dtpDate.Enabled = false;
            this.dtpDate.Location = new System.Drawing.Point(112, 133);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(200, 20);
            this.dtpDate.TabIndex = 5;
            // 
            // chboxDate
            // 
            this.chboxDate.AutoSize = true;
            this.chboxDate.Checked = true;
            this.chboxDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chboxDate.Location = new System.Drawing.Point(112, 110);
            this.chboxDate.Name = "chboxDate";
            this.chboxDate.Size = new System.Drawing.Size(108, 17);
            this.chboxDate.TabIndex = 6;
            this.chboxDate.Text = "date automatique";
            this.chboxDate.UseVisualStyleBackColor = true;
            this.chboxDate.CheckedChanged += new System.EventHandler(this.chboxDate_CheckedChanged);
            // 
            // lblDateVisite
            // 
            this.lblDateVisite.AutoSize = true;
            this.lblDateVisite.Location = new System.Drawing.Point(13, 182);
            this.lblDateVisite.Name = "lblDateVisite";
            this.lblDateVisite.Size = new System.Drawing.Size(92, 13);
            this.lblDateVisite.TabIndex = 7;
            this.lblDateVisite.Text = "Date de la visite : ";
            // 
            // dtpDateVisite
            // 
            this.dtpDateVisite.Location = new System.Drawing.Point(112, 176);
            this.dtpDateVisite.Name = "dtpDateVisite";
            this.dtpDateVisite.Size = new System.Drawing.Size(200, 20);
            this.dtpDateVisite.TabIndex = 8;
            // 
            // lblBilan
            // 
            this.lblBilan.AutoSize = true;
            this.lblBilan.Location = new System.Drawing.Point(13, 226);
            this.lblBilan.Name = "lblBilan";
            this.lblBilan.Size = new System.Drawing.Size(39, 13);
            this.lblBilan.TabIndex = 9;
            this.lblBilan.Text = "Bilan : ";
            // 
            // txtBilan
            // 
            this.txtBilan.Location = new System.Drawing.Point(112, 223);
            this.txtBilan.Multiline = true;
            this.txtBilan.Name = "txtBilan";
            this.txtBilan.Size = new System.Drawing.Size(200, 115);
            this.txtBilan.TabIndex = 10;
            // 
            // lblMotif
            // 
            this.lblMotif.AutoSize = true;
            this.lblMotif.Location = new System.Drawing.Point(13, 368);
            this.lblMotif.Name = "lblMotif";
            this.lblMotif.Size = new System.Drawing.Size(39, 13);
            this.lblMotif.TabIndex = 11;
            this.lblMotif.Text = "Motif : ";
            // 
            // cboxMotif
            // 
            this.cboxMotif.FormattingEnabled = true;
            this.cboxMotif.Location = new System.Drawing.Point(112, 365);
            this.cboxMotif.Name = "cboxMotif";
            this.cboxMotif.Size = new System.Drawing.Size(200, 21);
            this.cboxMotif.TabIndex = 12;
            // 
            // buttonAction
            // 
            this.buttonAction.Location = new System.Drawing.Point(33, 438);
            this.buttonAction.Name = "buttonAction";
            this.buttonAction.Size = new System.Drawing.Size(125, 23);
            this.buttonAction.TabIndex = 13;
            this.buttonAction.Text = "Enregistrer";
            this.buttonAction.UseVisualStyleBackColor = true;
            this.buttonAction.Click += new System.EventHandler(this.buttonAction_Click);
            // 
            // buttonRetour
            // 
            this.buttonRetour.Location = new System.Drawing.Point(180, 438);
            this.buttonRetour.Name = "buttonRetour";
            this.buttonRetour.Size = new System.Drawing.Size(132, 23);
            this.buttonRetour.TabIndex = 14;
            this.buttonRetour.Text = "Annuler";
            this.buttonRetour.UseVisualStyleBackColor = true;
            this.buttonRetour.Click += new System.EventHandler(this.buttonRetour_Click);
            // 
            // txtAutreMotif
            // 
            this.txtAutreMotif.Location = new System.Drawing.Point(112, 365);
            this.txtAutreMotif.Name = "txtAutreMotif";
            this.txtAutreMotif.Size = new System.Drawing.Size(200, 20);
            this.txtAutreMotif.TabIndex = 15;
            this.txtAutreMotif.Visible = false;
            // 
            // chboxAutreMotif
            // 
            this.chboxAutreMotif.AutoSize = true;
            this.chboxAutreMotif.Location = new System.Drawing.Point(112, 395);
            this.chboxAutreMotif.Name = "chboxAutreMotif";
            this.chboxAutreMotif.Size = new System.Drawing.Size(51, 17);
            this.chboxAutreMotif.TabIndex = 16;
            this.chboxAutreMotif.Text = "Autre";
            this.chboxAutreMotif.UseVisualStyleBackColor = true;
            this.chboxAutreMotif.CheckedChanged += new System.EventHandler(this.chboxAutreMotif_CheckedChanged);
            // 
            // txtPraticien
            // 
            this.txtPraticien.Location = new System.Drawing.Point(112, 58);
            this.txtPraticien.Name = "txtPraticien";
            this.txtPraticien.Size = new System.Drawing.Size(200, 20);
            this.txtPraticien.TabIndex = 17;
            this.txtPraticien.Visible = false;
            // 
            // CRForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.ClientSize = new System.Drawing.Size(348, 473);
            this.Controls.Add(this.txtPraticien);
            this.Controls.Add(this.chboxAutreMotif);
            this.Controls.Add(this.txtAutreMotif);
            this.Controls.Add(this.buttonRetour);
            this.Controls.Add(this.buttonAction);
            this.Controls.Add(this.cboxMotif);
            this.Controls.Add(this.lblMotif);
            this.Controls.Add(this.txtBilan);
            this.Controls.Add(this.lblBilan);
            this.Controls.Add(this.dtpDateVisite);
            this.Controls.Add(this.lblDateVisite);
            this.Controls.Add(this.chboxDate);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.cboxPraticien);
            this.Controls.Add(this.lblPraticien);
            this.Controls.Add(this.cboxCollaborateur);
            this.Controls.Add(this.lblCollaborateur);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CRForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ajouter un compte-rendu";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AjouterCRForm_FormClosing);
            this.Load += new System.EventHandler(this.AjouterCRForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCollaborateur;
        private System.Windows.Forms.ComboBox cboxCollaborateur;
        private System.Windows.Forms.Label lblPraticien;
        private System.Windows.Forms.ComboBox cboxPraticien;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.CheckBox chboxDate;
        private System.Windows.Forms.Label lblDateVisite;
        private System.Windows.Forms.DateTimePicker dtpDateVisite;
        private System.Windows.Forms.Label lblBilan;
        private System.Windows.Forms.TextBox txtBilan;
        private System.Windows.Forms.Label lblMotif;
        private System.Windows.Forms.ComboBox cboxMotif;
        private System.Windows.Forms.Button buttonAction;
        private System.Windows.Forms.Button buttonRetour;
        private System.Windows.Forms.TextBox txtAutreMotif;
        private System.Windows.Forms.CheckBox chboxAutreMotif;
        private System.Windows.Forms.TextBox txtPraticien;
    }
}