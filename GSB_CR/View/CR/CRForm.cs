﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GSB_CR.Model.DAO;
using GSB_CR.Model.Object;

namespace GSB_CR.View.CR
{
    public partial class CRForm : Form
    {
        //Propriétés
        List<string> groupes;
        string Collaborateur, Action;
        List<Collaborateur> collaborateurs;
        List<Praticien> praticiens;
        List<Motif> motifs;
        List<CompteRendu> compteRendus;

        public CRForm(List<string> groupes, string collaborateur, string action)
        {
            InitializeComponent();
            this.groupes = groupes;
            this.Collaborateur = collaborateur;
            Action = action;
        }

        //Retour au menu de compte-rendu
        private void buttonRetour_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Retour au menu de compte-rendu
        private void AjouterCRForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            MenuForm menu = new MenuForm(groupes, Collaborateur);
            menu.Show();
        }

        //Récupération de la liste des collaborateurs, des praticiens, des motifs
        private void AjouterCRForm_Load(object sender, EventArgs e)
        {
            PraticienDAO praticienDAO = new PraticienDAO();
            praticiens = praticienDAO.FindAll();
            MotifDAO motifDAO = new MotifDAO();
            motifs = motifDAO.FindAll();
            cboxMotif.DataSource = motifs;
            CollaborateurDAO collaborateurDAO = new CollaborateurDAO();
            collaborateurs = collaborateurDAO.FindAll();
            switch (Action)
            {
                case "Ajouter":
                    if (groupes.Contains("Visiteur"))
                        cboxCollaborateur.Enabled = false;
                    if (groupes.Contains("Responsable"))
                        cboxCollaborateur.Enabled = true;
                    cboxCollaborateur.DataSource = collaborateurs;
                    cboxCollaborateur.SelectedIndex = collaborateurs.IndexOf(collaborateurs.Find(x => x.getLogin() == Collaborateur));
                    cboxPraticien.DataSource = praticiens;
                    break;

                case "Consulter":
                    this.Text = "Consulter un compte-rendu";
                    cboxPraticien.Visible = false;
                    txtPraticien.Visible = true;
                    buttonAction.Visible = false;
                    lblPraticien.Visible = false;
                    txtPraticien.Visible = false;
                    cboxPraticien.Visible = false;
                    dtpDate.Enabled = false;
                    dtpDateVisite.Enabled = false;
                    chboxDate.Visible = false;
                    txtBilan.Enabled = false;
                    cboxMotif.Visible = false;
                    txtAutreMotif.Visible = true;
                    txtAutreMotif.Enabled = false;
                    chboxAutreMotif.Visible = false;
                    SetInfos();
                    break;

                case "Modifier":
                    this.Text = "Modifier un compte-rendu";
                    lblPraticien.Visible = false;
                    cboxPraticien.Visible = false;
                    SetInfos();
                    break;

                case "Supprimer":
                    this.Text = "Supprimer un compte-rendu";
                    cboxPraticien.Visible = false;
                    txtPraticien.Visible = false;
                    lblPraticien.Visible = false;
                    dtpDate.Enabled = false;
                    dtpDateVisite.Enabled = false;
                    chboxDate.Visible = false;
                    txtBilan.Enabled = false;
                    cboxMotif.Visible = false;
                    txtAutreMotif.Visible = true;
                    txtAutreMotif.Enabled = false;
                    chboxAutreMotif.Visible = false;
                    buttonAction.Text = "Supprimer";
                    SetInfos();
                    break;
            }
        }

        //Afficher les informations des compte-rendus
        private void SetInfos()
        {
            Collaborateur collaborateur = collaborateurs.Find(x => x.getLogin() == Collaborateur);
            CompteRenduDAO compteRenduDAO = new CompteRenduDAO();
            if (groupes.Contains("Visiteur"))
            {
                compteRendus = compteRenduDAO.ReadAllByCollaborateur(collaborateur);
                if(compteRendus.Count == 0)
                {
                    MessageBox.Show("Vous n'avez enregistré aucun compte-rendu.");
                    this.Close();
                }
            }
            if (groupes.Contains("Responsable"))
                compteRendus = compteRenduDAO.ReadAll();
            List<string> CRColPra = new List<string>();
            foreach (CompteRendu cr in compteRendus)
                CRColPra.Add(cr.getNum() + " -- " + cr.getCollaborateur().ToString() + " -- " + cr.getPraticien());
            cboxCollaborateur.DataSource = CRColPra;
        }

        //Modification de l'automatiste de la date
        private void chboxDate_CheckedChanged(object sender, EventArgs e)
        {
            if (chboxDate.Checked)
            {
                dtpDate.Enabled = false;
                dtpDate.Value = DateTime.Today;
            }
            else
                dtpDate.Enabled = true;
        }

        //Choisir ou personnaliser un motif
        private void chboxAutreMotif_CheckedChanged(object sender, EventArgs e)
        {
            if (chboxAutreMotif.Checked)
            {
                cboxMotif.Visible = false;
                txtAutreMotif.Visible = true;
            }
            else
            {
                cboxMotif.Visible = true;
                txtAutreMotif.Visible = false;
            }
        }

        //Affichage des informations du compte-rendu selon le collaborateur sélectionné
        private void cboxCollaborateur_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Action != "Ajouter")
            {
                CompteRendu cr = compteRendus.ElementAt(cboxCollaborateur.SelectedIndex);
                dtpDate.Value = cr.getDate();
                dtpDateVisite.Value = cr.getDateVisite();
                txtBilan.Text = cr.getBilan();
                txtAutreMotif.Text = cr.getMotif().ToString();
                cboxMotif.SelectedIndex = motifs.IndexOf(motifs.Find(x => x.getId() == cr.getIdMotif()));
            }
        }

        //Effectuer une action sur un compte-rendu
        private void buttonAction_Click(object sender, EventArgs e)
        {
            CompteRendu compteRendu;
            CompteRenduDAO compteRenduDAO = new CompteRenduDAO();
            Motif motif;
            switch (Action)
            {
                case "Ajouter":
                    if (txtAutreMotif.Visible)
                    {
                        motif = new Motif(0, txtAutreMotif.Text);
                        MotifDAO motifDAO = new MotifDAO();
                        motifDAO.Insert(motif);
                        motif.setId(motifDAO.LastInsertId());
                    }
                    else
                        motif = motifs.ElementAt(cboxMotif.SelectedIndex);
                    compteRendu = new CompteRendu(collaborateurs.ElementAt(cboxCollaborateur.SelectedIndex).getMatricule(), 0, praticiens.ElementAt(cboxPraticien.SelectedIndex).getNum(), dtpDate.Value, dtpDateVisite.Value, txtBilan.Text, motif.getId());
                    compteRenduDAO.Insert(compteRendu);
                    this.Close();
                    break;

                case "Modifier":
                    if (txtAutreMotif.Visible)
                    {
                        motif = new Motif(0, txtAutreMotif.Text);
                        MotifDAO motifDAO = new MotifDAO();
                        motifDAO.Insert(motif);
                        motif.setId(motifDAO.LastInsertId());
                    }
                    else
                        motif = motifs.ElementAt(cboxMotif.SelectedIndex);
                    CompteRendu cr = compteRendus.ElementAt(cboxCollaborateur.SelectedIndex);
                    compteRendu = new CompteRendu(cr.getMatricule(), cr.getNum(), cr.getNumPraticien(), dtpDate.Value, dtpDateVisite.Value, txtBilan.Text, motif.getId());
                    compteRenduDAO.Update(compteRendu);
                    this.Close();
                    break;

                case "Supprimer":
                    compteRendu = compteRendus.ElementAt(cboxCollaborateur.SelectedIndex);
                    compteRenduDAO.Delete(compteRendu);
                    this.Close();
                    break;
            }
        }
    }
}
