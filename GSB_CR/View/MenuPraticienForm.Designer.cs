﻿namespace GSB_CR.View
{
    partial class MenuPraticienForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuPraticienForm));
            this.buttonConsulterPraticien = new System.Windows.Forms.Button();
            this.buttonAjouterPraticien = new System.Windows.Forms.Button();
            this.buttonModifierPraticien = new System.Windows.Forms.Button();
            this.buttonSupprimerPraticien = new System.Windows.Forms.Button();
            this.buttonRetour = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonConsulterPraticien
            // 
            this.buttonConsulterPraticien.Image = ((System.Drawing.Image)(resources.GetObject("buttonConsulterPraticien.Image")));
            this.buttonConsulterPraticien.Location = new System.Drawing.Point(12, 12);
            this.buttonConsulterPraticien.Name = "buttonConsulterPraticien";
            this.buttonConsulterPraticien.Size = new System.Drawing.Size(90, 90);
            this.buttonConsulterPraticien.TabIndex = 2;
            this.buttonConsulterPraticien.UseVisualStyleBackColor = true;
            this.buttonConsulterPraticien.Click += new System.EventHandler(this.buttonConsulterPraticien_Click);
            // 
            // buttonAjouterPraticien
            // 
            this.buttonAjouterPraticien.Image = ((System.Drawing.Image)(resources.GetObject("buttonAjouterPraticien.Image")));
            this.buttonAjouterPraticien.Location = new System.Drawing.Point(108, 12);
            this.buttonAjouterPraticien.Name = "buttonAjouterPraticien";
            this.buttonAjouterPraticien.Size = new System.Drawing.Size(90, 90);
            this.buttonAjouterPraticien.TabIndex = 3;
            this.buttonAjouterPraticien.UseVisualStyleBackColor = true;
            this.buttonAjouterPraticien.Click += new System.EventHandler(this.buttonAjouterPraticien_Click);
            // 
            // buttonModifierPraticien
            // 
            this.buttonModifierPraticien.Image = ((System.Drawing.Image)(resources.GetObject("buttonModifierPraticien.Image")));
            this.buttonModifierPraticien.Location = new System.Drawing.Point(204, 12);
            this.buttonModifierPraticien.Name = "buttonModifierPraticien";
            this.buttonModifierPraticien.Size = new System.Drawing.Size(90, 90);
            this.buttonModifierPraticien.TabIndex = 4;
            this.buttonModifierPraticien.UseVisualStyleBackColor = true;
            this.buttonModifierPraticien.Click += new System.EventHandler(this.buttonModifierPraticien_Click);
            // 
            // buttonSupprimerPraticien
            // 
            this.buttonSupprimerPraticien.Image = ((System.Drawing.Image)(resources.GetObject("buttonSupprimerPraticien.Image")));
            this.buttonSupprimerPraticien.Location = new System.Drawing.Point(300, 12);
            this.buttonSupprimerPraticien.Name = "buttonSupprimerPraticien";
            this.buttonSupprimerPraticien.Size = new System.Drawing.Size(90, 90);
            this.buttonSupprimerPraticien.TabIndex = 5;
            this.buttonSupprimerPraticien.UseVisualStyleBackColor = true;
            this.buttonSupprimerPraticien.Click += new System.EventHandler(this.buttonSupprimerPraticien_Click);
            // 
            // buttonRetour
            // 
            this.buttonRetour.Image = ((System.Drawing.Image)(resources.GetObject("buttonRetour.Image")));
            this.buttonRetour.Location = new System.Drawing.Point(396, 12);
            this.buttonRetour.Name = "buttonRetour";
            this.buttonRetour.Size = new System.Drawing.Size(90, 90);
            this.buttonRetour.TabIndex = 6;
            this.buttonRetour.UseVisualStyleBackColor = true;
            this.buttonRetour.Click += new System.EventHandler(this.buttonRetour_Click);
            // 
            // MenuPraticienForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(502, 115);
            this.Controls.Add(this.buttonRetour);
            this.Controls.Add(this.buttonSupprimerPraticien);
            this.Controls.Add(this.buttonModifierPraticien);
            this.Controls.Add(this.buttonAjouterPraticien);
            this.Controls.Add(this.buttonConsulterPraticien);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MenuPraticienForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Praticien : Menu";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MenuPraticienForm_FormClosing);
            this.Load += new System.EventHandler(this.MenuPraticienForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonConsulterPraticien;
        private System.Windows.Forms.Button buttonAjouterPraticien;
        private System.Windows.Forms.Button buttonModifierPraticien;
        private System.Windows.Forms.Button buttonSupprimerPraticien;
        private System.Windows.Forms.Button buttonRetour;
    }
}