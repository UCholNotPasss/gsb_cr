﻿namespace GSB_CR.View
{
    partial class MenuMedicamentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuMedicamentForm));
            this.buttonQuitter = new System.Windows.Forms.Button();
            this.buttonModifierMed = new System.Windows.Forms.Button();
            this.buttonAjouterMed = new System.Windows.Forms.Button();
            this.buttonConsulterMed = new System.Windows.Forms.Button();
            this.buttonSupprimer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonQuitter
            // 
            this.buttonQuitter.Image = ((System.Drawing.Image)(resources.GetObject("buttonQuitter.Image")));
            this.buttonQuitter.Location = new System.Drawing.Point(396, 12);
            this.buttonQuitter.Name = "buttonQuitter";
            this.buttonQuitter.Size = new System.Drawing.Size(90, 90);
            this.buttonQuitter.TabIndex = 7;
            this.buttonQuitter.UseVisualStyleBackColor = true;
            this.buttonQuitter.Click += new System.EventHandler(this.buttonQuitter_Click);
            // 
            // buttonModifierMed
            // 
            this.buttonModifierMed.Image = ((System.Drawing.Image)(resources.GetObject("buttonModifierMed.Image")));
            this.buttonModifierMed.Location = new System.Drawing.Point(204, 12);
            this.buttonModifierMed.Name = "buttonModifierMed";
            this.buttonModifierMed.Size = new System.Drawing.Size(90, 90);
            this.buttonModifierMed.TabIndex = 6;
            this.buttonModifierMed.UseVisualStyleBackColor = true;
            this.buttonModifierMed.Click += new System.EventHandler(this.buttonModifierMed_Click);
            // 
            // buttonAjouterMed
            // 
            this.buttonAjouterMed.Image = ((System.Drawing.Image)(resources.GetObject("buttonAjouterMed.Image")));
            this.buttonAjouterMed.Location = new System.Drawing.Point(108, 12);
            this.buttonAjouterMed.Name = "buttonAjouterMed";
            this.buttonAjouterMed.Size = new System.Drawing.Size(90, 90);
            this.buttonAjouterMed.TabIndex = 5;
            this.buttonAjouterMed.UseVisualStyleBackColor = true;
            this.buttonAjouterMed.Click += new System.EventHandler(this.buttonAjouterMed_Click);
            // 
            // buttonConsulterMed
            // 
            this.buttonConsulterMed.Image = ((System.Drawing.Image)(resources.GetObject("buttonConsulterMed.Image")));
            this.buttonConsulterMed.Location = new System.Drawing.Point(12, 12);
            this.buttonConsulterMed.Name = "buttonConsulterMed";
            this.buttonConsulterMed.Size = new System.Drawing.Size(90, 90);
            this.buttonConsulterMed.TabIndex = 4;
            this.buttonConsulterMed.UseVisualStyleBackColor = true;
            this.buttonConsulterMed.Click += new System.EventHandler(this.buttonConsulterMed_Click);
            // 
            // buttonSupprimer
            // 
            this.buttonSupprimer.Image = ((System.Drawing.Image)(resources.GetObject("buttonSupprimer.Image")));
            this.buttonSupprimer.Location = new System.Drawing.Point(300, 12);
            this.buttonSupprimer.Name = "buttonSupprimer";
            this.buttonSupprimer.Size = new System.Drawing.Size(90, 90);
            this.buttonSupprimer.TabIndex = 8;
            this.buttonSupprimer.UseVisualStyleBackColor = true;
            this.buttonSupprimer.Click += new System.EventHandler(this.buttonSupprimer_Click);
            // 
            // MenuMedicamentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.ClientSize = new System.Drawing.Size(503, 115);
            this.Controls.Add(this.buttonSupprimer);
            this.Controls.Add(this.buttonQuitter);
            this.Controls.Add(this.buttonModifierMed);
            this.Controls.Add(this.buttonAjouterMed);
            this.Controls.Add(this.buttonConsulterMed);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MenuMedicamentForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Médicament : Menu";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MenuMedicamentForm_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonQuitter;
        private System.Windows.Forms.Button buttonModifierMed;
        private System.Windows.Forms.Button buttonAjouterMed;
        private System.Windows.Forms.Button buttonConsulterMed;
        private System.Windows.Forms.Button buttonSupprimer;
    }
}