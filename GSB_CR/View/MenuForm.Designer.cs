﻿namespace GSB_CR.View
{
    partial class MenuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuForm));
            this.buttonCR = new System.Windows.Forms.Button();
            this.buttonCollaborateur = new System.Windows.Forms.Button();
            this.buttonPraticien = new System.Windows.Forms.Button();
            this.buttonMedicament = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonCR
            // 
            this.buttonCR.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.buttonCR.Image = ((System.Drawing.Image)(resources.GetObject("buttonCR.Image")));
            this.buttonCR.Location = new System.Drawing.Point(12, 12);
            this.buttonCR.Name = "buttonCR";
            this.buttonCR.Size = new System.Drawing.Size(75, 75);
            this.buttonCR.TabIndex = 0;
            this.buttonCR.UseVisualStyleBackColor = false;
            this.buttonCR.Click += new System.EventHandler(this.buttonCR_Click);
            // 
            // buttonCollaborateur
            // 
            this.buttonCollaborateur.BackColor = System.Drawing.Color.Silver;
            this.buttonCollaborateur.Image = ((System.Drawing.Image)(resources.GetObject("buttonCollaborateur.Image")));
            this.buttonCollaborateur.Location = new System.Drawing.Point(93, 12);
            this.buttonCollaborateur.Name = "buttonCollaborateur";
            this.buttonCollaborateur.Size = new System.Drawing.Size(75, 75);
            this.buttonCollaborateur.TabIndex = 1;
            this.buttonCollaborateur.UseVisualStyleBackColor = false;
            this.buttonCollaborateur.Click += new System.EventHandler(this.buttonCollaborateur_Click);
            // 
            // buttonPraticien
            // 
            this.buttonPraticien.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.buttonPraticien.Image = ((System.Drawing.Image)(resources.GetObject("buttonPraticien.Image")));
            this.buttonPraticien.Location = new System.Drawing.Point(12, 93);
            this.buttonPraticien.Name = "buttonPraticien";
            this.buttonPraticien.Size = new System.Drawing.Size(75, 75);
            this.buttonPraticien.TabIndex = 2;
            this.buttonPraticien.UseVisualStyleBackColor = false;
            this.buttonPraticien.Click += new System.EventHandler(this.buttonPraticien_Click);
            // 
            // buttonMedicament
            // 
            this.buttonMedicament.BackColor = System.Drawing.Color.SkyBlue;
            this.buttonMedicament.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMedicament.Image = ((System.Drawing.Image)(resources.GetObject("buttonMedicament.Image")));
            this.buttonMedicament.Location = new System.Drawing.Point(93, 93);
            this.buttonMedicament.Name = "buttonMedicament";
            this.buttonMedicament.Size = new System.Drawing.Size(75, 75);
            this.buttonMedicament.TabIndex = 3;
            this.buttonMedicament.UseVisualStyleBackColor = false;
            this.buttonMedicament.Click += new System.EventHandler(this.buttonMedicament_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.button1.Location = new System.Drawing.Point(12, 174);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(156, 75);
            this.button1.TabIndex = 4;
            this.button1.Text = "Formulaire de Satisfaction";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(180, 263);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonMedicament);
            this.Controls.Add(this.buttonPraticien);
            this.Controls.Add(this.buttonCollaborateur);
            this.Controls.Add(this.buttonCR);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MenuForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Boîte à outils";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MenuForm_FormClosing);
            this.Load += new System.EventHandler(this.MenuForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonCR;
        private System.Windows.Forms.Button buttonCollaborateur;
        private System.Windows.Forms.Button buttonPraticien;
        private System.Windows.Forms.Button buttonMedicament;
        private System.Windows.Forms.Button button1;
    }
}