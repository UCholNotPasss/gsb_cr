﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSB_CR.View
{
    public partial class MenuCollaborateurForm : Form
    {
        //Propriétés
        List<string> groupes;
        string Collaborateur;

        public MenuCollaborateurForm(List<string> groupes, string collaborateur)
        {
            InitializeComponent();
            this.groupes = groupes;
            this.Collaborateur = collaborateur;
        }

        //Retour à la boite à outils
        private void buttonQuitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //Retour à la boite à outils
        private void MenuCollaborateurForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            MenuForm menu = new MenuForm(groupes,Collaborateur);
            menu.Show();
        }

        //Affichage de la page de consultation / blocage des collaborateurs
        private void buttonConsulterCollaborateur_Click(object sender, EventArgs e)
        {
            Collaborateurs.CollaborateurForm collaborateurForm = new Collaborateurs.CollaborateurForm(groupes, Collaborateur, "Consulter");
            collaborateurForm.Show();
            this.Hide();
        }

        //Affichage de la page d'ajout d'un collaborateur
        private void buttonAjouterCollaborateur_Click(object sender, EventArgs e)
        {
            Collaborateurs.CollaborateurForm collaborateurForm = new Collaborateurs.CollaborateurForm(groupes, Collaborateur, "Ajouter");
            collaborateurForm.Show();
            this.Hide();
        }

        //Affichage de la page de modification d'un collaborateur
        private void buttonModifier_Click(object sender, EventArgs e)
        {
            Collaborateurs.CollaborateurForm collaborateurForm = new Collaborateurs.CollaborateurForm(groupes, Collaborateur, "Modifier");
            collaborateurForm.Show();
            this.Hide();
        }

        //Affichage de la page de suppression d'un collaborateur
        private void buttonSupprimer_Click(object sender, EventArgs e)
        {
            Collaborateurs.CollaborateurForm collaborateurForm = new Collaborateurs.CollaborateurForm(groupes, Collaborateur, "Supprimer");
            collaborateurForm.Show();
            this.Hide();
        }
    }
}
